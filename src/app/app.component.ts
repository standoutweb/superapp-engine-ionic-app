import { Component, } from '@angular/core';

import { AlertController, Events, NavController, Platform, } from '@ionic/angular';

// Native plugins
import { Badge } from '@ionic-native/badge/ngx';
import { Push, PushObject, RegistrationEventResponse, NotificationEventResponse, } from '@ionic-native/push/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

// Other 3rd party
import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';
import { TranslateService } from '@ngx-translate/core';

// Internal
import { config } from './config';
import { AuthService } from './services/auth/auth.service';
import { DataService } from './services/data/data.service';
import { HelpersService } from './services/helpers/helpers.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  private rootPage: any;

  private group: sae.IGroup;
  styles: string = '';
  config: sae.IAttributeValues = {};

  private sections: Array<sae.IDestination> = [];

  private designatedSection: sae.IDestination;

  private badges = {};
  private user: any = {};
  private totalUnanswered: number = 0;
  private groups: Array<sae.IGroup> = [];
  private revealGroups: boolean = false;

  constructor(
    public alertCtrl: AlertController,
    public events: Events,
    public navCtrl: NavController,
    public platform: Platform,

    public badge: Badge,
    public push: Push,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,

    public translate: TranslateService,
    public auth: AuthService,
    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('MyApp::constructor()');
    this.platform.ready().then(() => this.platformDidReady());
  }

  private platformDidReady() {
    // console.log('platformDidReady()');
    // Okay, so the platform is ready and our plugins are available.
    // Here you can do any higher level native things you might need.

    console.info('appVersion', config.appVersion);

    // TODO: Transparent or top-pixel matched color
    // statusBar.styleBlackTranslucent();
    // statusBar.backgroundColorByName('red');
    this.statusBar.styleDefault();

    this.platform.resume.subscribe(
      () => {
        // console.log('platform.resume');

        if (this.group) {
          this.data.getGroup(this.group.id).subscribe(
            (group: sae.IGroup) => this.events.publish('group:switched', group),
            (err) => console.error(err),
          );
        }

        this.badge.clear();
      }
    );

    this.events.subscribe(
      'rootPage:loaded',
      () => {
        // console.log('rootPageLoaded');
        this.registerAndWatchPush();
      }
    );

    this.loadUser();
    this.events.subscribe('user:loaded', (auUser: sae.IAuthUser) => this.userDidLoad(auUser));

    this.data.state.menuEnabled = false;

    this.events.subscribe('section:open', (auSection: sae.IDestination) => this.resolveOpenSection(auSection));
    this.events.subscribe('section:landing', () => this.openLandingSection());

    this.translate.setDefaultLang(config.defaultLang);

    // Include group configs
    this.events.subscribe('group:switched', (group: sae.IGroup) => {
      // console.log('group', group);
      var language_iso = config.defaultLang;

      if (group) {
        this.data.setActiveGroup(group);
        this.group = group;
        this.config = group.attributes;
        this.styles = this.renderStyles(group.attributes);
        if (group.attributes.language_iso) {
          language_iso = String(group.attributes.language_iso);
        }

        // update side menu when sections loaded
        this.data.getSections().subscribe(
          (sections: Array<sae.IDestination>) => {
            // console.log('sections', sections);
            if (!sections) return;
            this.sections = sections;
            this.openLandingSection(true);
            this.openDesignatedSection();
            // Badges in menu for dynamic sections
            this.countSectionBadges(sections);
          }
        );

      } else {
        this.group = null;
        this.config = null;
        this.styles = '';
        this.rootPage = null;
      }
      this.translate.setDefaultLang(language_iso);
    });

    this.events.subscribe('logout', (params) => {
      // console.log('events.subscribe("logout", (params) => {})', params);
      this.splashScreen.show();

      this.helpers.timePresentLoading('');
      this.auth.logOut()
        .finally(() => this.helpers.timeDismissLoading())
        .finally(() => {
          // Clear the applied group styles on logout
          this.events.publish('group:switched', null);
          // navigate to Welcome section
          this.navCtrl.navigateRoot('/welcome', { queryParams: params });
        })
        .subscribe();
    });

    this.data.listGroups()
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (groups: Array<sae.IGroup>) => {
          // console.log('groups', groups);
          this.groups = groups;
          var glen = groups.length;
          var gcount = 0;
          for (var gi = 0; gi < glen; gi++) {
            if (groups[gi].public || (groups[gi] as any).is_member) {
              gcount++;
            }
          }
          // console.log('gcount', gcount);
          // check if only one visibly listed 
          if (gcount > 1) { this.revealGroups = true; }
        },
        (err) => console.error(err)
      );
    this.helpers.timePresentLoading('');
  }

  private registerAndWatchPush() {
    // console.log('registerAndWatchPush()');
    const pushObject: PushObject = this.push.init(config.pushOptions);

    pushObject.on('registration').subscribe(
      (registration: RegistrationEventResponse) => {
        // console.log('registration', registration);
        this.data.setPushToken(registration.registrationId).subscribe(
          () => this.events.publish('pushToken:saved')
        );
      }
    );

    pushObject.on('notification').subscribe(
      (message: NotificationEventResponse) => {
        this.handlePushMessage(message);
      }
    );

    pushObject.on('error').subscribe(
      (error) => console.error('Error with Push plugin', error)
    );
  }

  private async handlePushMessage(message: NotificationEventResponse) {
    // console.log('handlePushMessage(message)', message);
    // need to deep-copy because PushMessage interface does not account
    var payload = message.additionalData.payload;
    if (typeof payload == 'string') {
      payload = JSON.parse(payload);
    }
    // console.log('payload', payload);
    // handle login push
    if (payload && payload.token) {
      // console.log('log in with token', payload.token);
      this.events.publish('loginpush', payload.token);
      return;
    }

    this.data.pullMessages();

    // just go to messages
    if (!payload || !payload.destination || !payload.destination.length) {
      this.designatedSection = config.localSections.find((section) => section.component === 'MessageListPage');
    } else {
      // TODO: Handle multiple destinations
      let destination: sae.IDestination = payload.destination[0];
      if (typeof destination.params == 'string') {
        destination.params = JSON.parse(destination.params);
      }
      this.designatedSection = destination;
    }
    // console.log('this.designatedSection', this.designatedSection);
    // show alert prompt if app in foreground
    if (message.additionalData.foreground) {
      // console.log('foreground');
      var alertOpts = {
        title: this.translate.instant('Would you like to open this message?'),
        message: message.title,
        buttons: [
          {
            text: this.translate.instant('Cancel'),
          },
          {
            text: this.translate.instant('Open'),
            handler: () => this.openDesignatedSection()
          }
        ],
        enableBackdropDismiss: false,
      };
      const alert = await this.alertCtrl.create(alertOpts);
      await alert.present();
    } else {
      // console.log('not foreground');
      this.openDesignatedSection();
    }
    // console.log('this.rootPage', this.rootPage);
  }

  private loadUser() {
    // console.log('loadUser()');
    this.data.getUser().subscribe(
      (user: sae.IAuthUser) => this.events.publish('user:loaded', user),
      (err) => console.error(err)
    );

    this.auth.user().subscribe(
      (user: sae.IAuthUser) => this.events.publish('user:loaded', user),
      (err) => {
        // console.error(err);
        err = JSON.parse(JSON.stringify(err));
        console.error(err);
        if (err.response && err.response.status == 401) {
          this.events.publish('logout');
        }
      }
    );
  }

  private userDidLoad(auUser: sae.IAuthUser) {
    // console.log('userDidLoad(auUser)', auUser);

    if (auUser && auUser.token) {
      this.user = auUser;

      this.data.setUser(auUser);
    }

    if (this.rootPage) {
      // console.log('this.rootPage already set - exit', this.rootPage);
      return;
    }

    if (auUser && auUser.token) {
      // console.log('authToken present - go to groups');
      this.data.getActiveGroup().then(
        (activeGroup: sae.IGroup) => {
          // console.log('activeGroup', activeGroup);
          if (this.rootPage) {
            // console.log('this.rootPage already set - exit', this.rootPage);
            return;
          }
          if (activeGroup && activeGroup.id) {

            // console.log('activeGroup present - go to dashbaord');
            this.events.publish('group:switched', activeGroup);
          } else {
            this.rootPage = 'GroupsPage';
            this.navCtrl.navigateRoot('/groups');
          }
          // console.log('this.rootPage', this.rootPage);
        },
        (err) => {
          console.error(err);
          this.rootPage = 'GroupsPage';
          this.navCtrl.navigateRoot('/groups');
        }
      );
    } else {
      this.rootPage = 'WelcomePage';
      this.navCtrl.navigateRoot('/welcome');
    }
    // console.log('this.rootPage', this.rootPage);
  }

  openSection(sectionConf: sae.IDestination) {
    // console.log('openSection(sectionConf)', sectionConf);
    this.events.publish('section:open', sectionConf);
  }

  private resolveOpenSection(sectionConf: sae.IDestination) {
    // console.log('resolveOpenSection(sectionConf)', sectionConf);
    // console.log('config.handledPlugins', config.handledPlugins);

    if (!config.handledPlugins[sectionConf.component]) {
      this.helpers.handleError('Unhandled component ' + sectionConf.component)
      return;
    }
    if (!this.rootPage) {
      this.rootPage = config.handledPlugins[sectionConf.component];
    }

    var path = config.handledPlugins[sectionConf.component];
    if (path != '/groups') {
      path += '/' + sectionConf.id;
    }
    this.navCtrl.navigateForward(path);
  }

  openProfileSection() {
    // console.log('openProfileSection()');
    this.navCtrl.navigateForward('/profile');
  }

  private openLandingSection(initial?: boolean) {
    // console.log('openLandingSection(initial)', initial);
    if (initial && this.rootPage) return;
    if (!this.sections.length) return;

    this.splashScreen.hide();

    let landingSection = this.sections.find(
      (sec) => sec.enabled
    );
    // console.log('this.sections', this.sections);
    // console.log('landingSection', landingSection);

    this.resolveOpenSection(landingSection);
  }

  private openDesignatedSection() {
    // console.log('openDesignatedSection()');
    if (!this.designatedSection) return;
    var designatedSection = this.designatedSection;

    if (designatedSection.id) {
      this.designatedSection = null;
      this.resolveOpenSection(designatedSection);
      return;
    }

    // otherwise wait for this.sections

    if (!this.sections.length) return;

    var firstMatchSec = this.sections.find(
      (sec: sae.IDestination) => (sec.component == designatedSection.component)
    );
    // console.log('matches', matches);

    if (!firstMatchSec) return;
    this.designatedSection = null;
    this.resolveOpenSection(firstMatchSec);
  }

  private countSectionBadges(sections: Array<sae.IDestination>) {
    // console.log('countSectionBadges(sections)', sections);
    sections.forEach(
      (section) => {
        switch (section.component) {
          case 'MessageListPage':
            this.data.getMessages().subscribe(
              (auMessages: Array<sae.IMessageLike>) => {
                // console.log('auMessages', auMessages);
                if (!auMessages) return;
                this.badges[section.id] = this.countMessageListBadge(auMessages);
                // console.log('this.badges[section.id]', this.badges[section.id]);
              }
            );
            break;

          default:
            // console.info('TODO: count badges for', section.id)
            break;
        }
      }
    );

    this.countQuestionsBadges(sections.filter((sec) => sec.component == 'QuestionPage'));
  }

  private countMessageListBadge(messages: sae.IMessageLike[]): number {
    // console.log('countMessageListBadge(messages)', messages);

    let count = messages.filter(
      (msg) => {
        if (msg.last_read) return false;
        if (msg.template_attributes.is_hidden) return false;
        return true;
      }
    ).length;

    return count;
  }

  private countQuestionsBadges(sections: sae.IDestination[]) {
    // console.log('countQuestionsBadges(sections)', sections);
    this.data.getQuestionSets().subscribe(
      (qSets: Array<sae.ICategory>) => {
        // console.log('qSets', qSets);

        this.totalUnanswered = 0;
        if (!qSets) {
          // TODO: Smth smarter here. Stop propagation?
          return;
        }
        qSets.forEach(
          (qSet: sae.ICategory) => {
            // console.log('qSet', qSet);
            this.totalUnanswered += Number((qSet as any).unanswered_questions);

            sections.forEach(
              (sec) => {
                // console.log('sec', sec);
                if (sec.params.user_attribute_category_id == qSet.id) {
                  this.badges[sec.id] = Number((qSet as any).unanswered_questions);
                }
              }
            );
            // console.log('this.badges', this.badges);
          }
        );
      }
    );
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    // App config scripts
    if (config.font_source) {
      scripts = scripts.concat('<link href="' + config.font_source + '" rel="stylesheet">');
    }

    // App config styles
    if (config.primary_color) {
      css = css.concat(
        'a, .icon[color="primary"], .menu-inner .content .list .item-block.active .icon, table strong, ' +
        '.menu-inner .content .list button .icon, .menu-inner .content .list button.activated .icon, .item.item-radio-checked ion-label {color: ' + config.primary_color + '}' +
        '.button, .button[color="primary"] {background-color: ' + config.primary_color + '}' +
        '.checkbox .checkbox-checked {border-color: ' + config.primary_color + '; background-color: ' + config.primary_color + '}' +
        '.radio-md .radio-icon {border-color: ' + config.primary_color + '}' +
        '.radio-md .radio-checked .radio-inner {background-color: ' + config.primary_color + '}' +
        '.radio-ios .radio-checked .radio-inner {border-color: ' + config.primary_color + '}'
      );
    }

    if (config.primary_color_contrast) {
      css = css.concat('.button {color: ' + config.primary_color_contrast + '}');
    }

    if (config.danger_color) {
      css = css.concat('[color="danger"] {background-color: ' + config.danger_color + '!important}');
    }

    if (config.success_color) {
      css = css.concat('ion-badge[color="primary"] {background-color: ' + config.success_color + '}');
    }

    if (config.h1_color) {
      css = css.concat('h1 {color: ' + config.h1_color + '}');
    }

    if (config.h1_font_size) {
      css = css.concat('h1 {font-size: ' + config.h1_font_size + '}');
    }

    if (config.h1_margin) {
      css = css.concat('h1 {margin: ' + config.h1_margin + '}');
    }

    if (config.price_color) {
      css = css.concat('.price {color: ' + config.price_color + '!important;}');
    }

    if (config.font_family) {
      css = css.concat('ion-app.app-root {font-family: ' + config.font_family + '}');
    }

    if (config.main_font_fontface_name && config.main_font_fontface_source) {
      css = css.concat('@font-face {font-family: ' + config.main_font_fontface_name + '; src: url(' + config.main_font_fontface_source + '.woff) format("woff"), url(' + config.main_font_fontface_source + '.ttf) format("truetype")}');
      css = css.concat('ion-app.app-root, page-chapter-list h2 {font-family: ' + config.main_font_fontface_name + '}');
    }

    if (config.headers_font_fontface_name && config.headers_font_fontface_source) {
      css = css.concat('@font-face {font-family: ' + config.headers_font_fontface_name + '; src: url(' + config.headers_font_fontface_source + '.woff) format("woff"), url(' + config.headers_font_fontface_source + '.ttf) format("truetype")}');
      css = css.concat('ion-menu, h1, h2, h3, h4, .published_at {font-family: ' + config.headers_font_fontface_name + '}');
    }

    if (config.menu_background) {
      css = css.concat('.menu-inner .content {background-image: url(' + config.menu_background + ')}');
    }

    if (config.menu_background_overlay_color) {
      css = css.concat('.menu-inner .content:before, .menu-inner .content .list .item-block.activated, .menu-inner .content .list .item-block.active {background-color: ' + config.menu_background_overlay_color + '}');
    }

    if (config.menu_text_color) {
      css = css.concat('.menu-inner .content .list .item-block, .menu-inner .content .item-profile .item-inner h2 {color: ' + config.menu_text_color + '}');
    }

    if (config.menu_text_color_active) {
      css = css.concat('.menu-inner .content .list .item-block.active .item-inner, .menu-inner .content .list .item-block.activated .item-inner {color: ' + config.menu_text_color_active + '}');
    }

    if (config.menu_icon_color) {
      css = css.concat('.menu-inner .content .list .item-block .icon {color: ' + config.menu_icon_color + '}');
    }

    if (config.menu_icon_color_active) {
      css = css.concat('.menu-inner .content .list .item-block.active .icon, .menu-inner .content .list .item-block.activated .icon {color: ' + config.menu_icon_color_active + '}');
    }

    if (config.menu_item_background_active) {
      css = css.concat('.menu-inner .content .list .item-block.activated, .menu-inner .content .list .item-block.active {background-color: ' + config.menu_item_background_active + '}');
    }

    if (config.menu_border_color) {
      css = css.concat('.menu-inner .content .list .item-block {border-color: ' + config.menu_border_color + '}');
    }

    if (config.menu_text_transform) {
      css = css.concat('.menu-inner .content .list button {text-transform: ' + config.menu_text_transform + '}');
    }

    if (config.menu_username_text_color) {
      css = css.concat('.menu-inner .content .item-profile .item-inner h2 {color: ' + config.menu_username_text_color + '}');
    }

    if (config.inside_page_background_image) {
      css += `
        .ion-page {
          background-image: url("${config.inside_page_background_image}");
        }
      `;
    }

    if (config.inside_page_background_color) {
      css += `
        .ion-page {
          background-color: ${config.inside_page_background_color};
        }
      `;
    }

    if (config.inside_page_background_overlay_color) {
      css += `
        .ion-page:before {
          background: ${config.inside_page_background_overlay_color};
        }
      `;
    }

    if (config.inside_page_bottom_padding) {
      css = css.concat('page-chapter .content .scroll-content .main_wrapper .content_wrapper, page-single-content .content .scroll-content .main_wrapper .content_wrapper {padding-bottom: ' + config.inside_page_bottom_padding + '}');
    }

    if (config.text_transform) {
      css = css.concat('h1, page-dashboard .item h3, .published_at {text-transform: ' + config.text_transform + '}');
    }

    if (config.published_at_color) {
      css = css.concat('.published_at {color: ' + config.published_at_color + '}');
    }

    if (config.article_list_item_border_top) {
      css = css.concat('page-chapter-list .item {border-top: ' + config.article_list_item_border_top + '}');
    }

    if (config.article_list_item_border_color) {
      css = css.concat('page-chapter-list .item {border-color: ' + config.article_list_item_border_color + '!important}');
    }

    if (config.article_list_image_border_radius) {
      css = css.concat('page-chapter-list .content .scroll-content ion-thumbnail img, page-chapter-list .content .scroll-content .icon img {border-radius: ' + config.article_list_image_border_radius + '}');
    }

    if (config.questionary_button_background) {
      css = css.concat('.button[color="pale"] {background-color: ' + config.questionary_button_background + '}');
    }

    if (config.header_background) {
      css = css.concat('ion-header {background-color: ' + config.header_background + '}');
    }

    if (config.header_burger_size) {
      css = css.concat('ion-header .icon {font-size: ' + config.header_burger_size + '}');
    }

    if (config.header_text_color) {
      css = css.concat('.header .toolbar-title, .header button {color: ' + config.header_text_color + '}');
    }

    if (config.styles) {
      css = css.concat(config.styles);
    }

    if (config.date_button_height) {
      css = css.concat('page-chapter-list .list .button, page-chapter .content .button {height: ' + config.date_button_height + '}');
    }

    if (config.date_button_color) {
      css = css.concat('page-chapter-list .list .button, page-chapter .content .button {background-color: ' + config.date_button_color + '}');
    }

    if (config.date_button_padding) {
      css = css.concat('page-chapter-list .list .button, page-chapter .content .button {padding: ' + config.date_button_padding + '}');
    }

    return this.applyStyles(scripts, css);
  }

  applyStyles(scripts, css) {
    return scripts +
      '<style>' + css + '</style>'
      ;
  }
}
