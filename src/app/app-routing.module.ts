import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full',
  },
  { path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomePageModule), },
  { path: 'groups', loadChildren: () => import('./pages/groups/groups.module').then(m => m.GroupsPageModule), },
  { path: 'groups/:groupid', loadChildren: () => import('./pages/groups/groups.module').then(m => m.GroupsPageModule), },

  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },

  { path: 'article-list/:secid', loadChildren: './pages/article-list/article-list.module#ArticleListPageModule' },
  { path: 'article-list/:secid/:catid', loadChildren: './pages/article-list/article-list.module#ArticleListPageModule' },
  { path: 'article/:secid', loadChildren: './pages/article/article.module#ArticlePageModule' },
  { path: 'article/:secid/:artid', loadChildren: './pages/article/article.module#ArticlePageModule' },
  { path: 'catalog-list/:secid', loadChildren: './pages/catalog-list/catalog-list.module#CatalogListPageModule' },
  { path: 'category-list/:secid', loadChildren: './pages/category-list/category-list.module#CategoryListPageModule' },
  { path: 'category-list/:secid/:catid', loadChildren: './pages/category-list/category-list.module#CategoryListPageModule' },
  { path: 'dashboard/:secid', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule', },
  { path: 'deposit-list/:secid', loadChildren: './pages/deposit-list/deposit-list.module#DepositListPageModule' },
  { path: 'message-list/:secid', loadChildren: './pages/message-list/message-list.module#MessageListPageModule' },
  { path: 'message/:secid', loadChildren: './pages/message/message.module#MessagePageModule' },
  { path: 'message/:secid/:mesid', loadChildren: './pages/message/message.module#MessagePageModule' },
  { path: 'question-list/:secid', loadChildren: './pages/question-list/question-list.module#QuestionListPageModule' },
  { path: 'question-list/:secid/:catid', loadChildren: './pages/question-list/question-list.module#QuestionListPageModule' },
  { path: 'question-report/:secid', loadChildren: './pages/question-report/question-report.module#QuestionReportPageModule' },
  { path: 'ticket-list/:secid', loadChildren: './pages/ticket-list/ticket-list.module#TicketListPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
