import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

export var config = {
    appVersion: '3.0.0',

    saeServer: {
        protocol: 'https',
        host: 'api.webapp.standoutweb.com',

        timeout: 18000,
    } as sae.IAPISpecs,

    features: {
        welcomeWelcome: true,
        welcomeRecognizePhone: true,
        welcomeRecognizeEmail: true,
        welcomeVerificationCode: true,
        welcomeCreateEmailPhone: true,

        // before enabling check if plugin installed
        welcomeFacebook: true,
        welcomeGuest: true,
    },

    defaultLang: 'en',

    handledPlugins: {
        GroupsPage: '/groups',
        DashboardPage: '/dashboard',
        MessageListPage: '/message-list',
        CatalogListPage: '/catalog-list',
        CategoryListPage: '/category-list',
        ArticleListPage: '/article-list',
        ArticlePage: '/article',
        QuestionPage: '/question-list',
        TicketListPage: '/ticket-list',
        InventoryPage: '/deposit-list',
        QuestionaryReportPage: '/question-report',
    },

    localSections: [
        {
            name: 'Groups',
            id: -1,
            icon: 'fas globe',
            component: 'GroupsPage',
            order: 100,
            params: {},
            // by default don't show - BE will override
            enabled: true,
        },
        {
            name: 'Messages',
            id: -2,
            icon: 'fas bell',
            component: 'MessageListPage',
            order: 1,
            params: {},
            // by default don't show - BE will override
            enabled: true,
        },
        {
            name: 'Catalogs',
            id: -3,
            icon: 'fas sitemap',
            component: 'CatalogListPage',
            order: 99,
            params: {},
            // by default don't show - BE will override
            enabled: false,
        },
        {
            name: 'Dashboard',
            id: -4,
            icon: 'fas th',
            component: 'DashboardPage',
            order: 0,
            params: {},
            // by default don't show - BE will override
            enabled: false,
        },
    ] as Array<sae.IDestination>,

    handledAttributeTypes: {
        options: true,
    },
    handledAttributeTypesDisplay: {
        options: true,
        varchar: true,
        boolean: true,
    },

    pushOptions: {
        ios: {
            badge: true,
            sound: true,
        },
        android: {
            senderID: '259609975724',
            iconColor: '#343434',
        },
    },

    facebookPermissions: [
        'public_profile',
        'email',
    ],
}
