import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'trustHtml',
})
export class TrustHtmlPipe implements PipeTransform {

  constructor(
    private sanitizer: DomSanitizer,
  ) {
    // console.log('TrustHtmlPipe::constructor()');
  }

  transform(value: string) {
    // console.log('transform(value)', value);
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

}
