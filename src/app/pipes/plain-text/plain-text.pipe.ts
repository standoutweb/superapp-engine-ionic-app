import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'plainText',
})
export class PlainTextPipe implements PipeTransform {
  /**
   * Strips HTML tags
   */
  transform(text) {
    // console.log('transform(text)', text);
    return text ? String(text).replace(/<[^>]+>/gm, '') : '';
  }
}
