import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitTo',
})
export class LimitToPipe implements PipeTransform {
  /**
   * Limit text to length
   */
  transform(text: string, len: string): string {
    let limit = len ? parseInt(len, 10) : 10;
    let trail = '...';

    return text.length > limit ? text.substring(0, limit) + trail : text;
  }
}
