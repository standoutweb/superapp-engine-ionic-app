import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FaSplitPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'faSplit',
})
export class FaSplitPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    if(!value) {
      return ['fas', 'square'];
    }
    let parts: string[] = value.split(' ');
    if (parts.length == 1) {
      parts.unshift('fas');
    }
    return parts;
  }
}
