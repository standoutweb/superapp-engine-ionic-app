import { NgModule } from '@angular/core';
import { PlainTextPipe } from './plain-text/plain-text.pipe';
import { LimitToPipe } from './limit-to/limit-to.pipe';
import { TrustHtmlPipe } from './trust-html/trust-html.pipe';
import { FaSplitPipe } from './fa-split/fa-split.pipe';

@NgModule({
  declarations: [
    PlainTextPipe,
    LimitToPipe,
    TrustHtmlPipe,
    FaSplitPipe,
  ],
  imports: [],
  exports: [
    PlainTextPipe,
    LimitToPipe,
    TrustHtmlPipe,
    FaSplitPipe,
  ]
})
export class PipesModule { }
