import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';

import { Device } from '@ionic-native/device/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

import { config } from '../../config';
import { DataService } from '../../services/data/data.service';
import { SuperappEngineApiService } from '../../services/superapp-engine-api/superapp-engine-api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private storage: Storage,
    public facebook: Facebook,
    private device: Device,
    public data: DataService,
    public superapp: SuperappEngineApiService,
  ) {
    // console.log('AuthService::constructor()');
  }

  public user(): Observable<sae.IAuthUser> {
    // console.log('user()');
    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);
        if (!authToken) {
          return Observable.throw({ response: { message: 'Authentication token not found', status: 401 } });
        }
        return this.superapp.setToken(authToken).Auth.me({ with_attributes: 1 }).map(
          // workaroud - stick the athentication token back to object
          (user) => Object.assign(user, { token: authToken })
        );
      }
    );
  }

  public registerWithEmailAndPhone(regData: sae.IAuthRegisterSpecs): Observable<sae.IAuthUser> {
    // console.log('registerWithEmailAndPhone(regData)', regData);
    if (regData.terms_accepted_at === 'now') {
      // 2015-10-26T07:46:36.611Z -> 2015-10-26T07:46:36
      regData.terms_accepted_at = new Date().toJSON().substr(0, 19);
    }
    return this.superapp.Auth.register(regData).do(
      (user) => this.createPushToken(user.token)
    );
  }

  public logInWithFacebook(regData: any): Observable<sae.IAuthUser> {
    // console.log('logInWithFacebook(regData)', regData);

    return Observable.fromPromise(this.data.getPushToken()).mergeMap(
      (pushToken) => {
        // console.log('pushToken', pushToken);
        regData.pushToken = pushToken;
        regData.platform = this.device.platform;
        regData.deviceid = this.device.uuid;
        regData.version = config.appVersion;
        return this.resolveFacebookLogin().mergeMap(
          (facebookAuthRes) => {
            // console.log('facebookAuthRes', facebookAuthRes);
            regData.facebookAccessToken = facebookAuthRes.accessToken;
            regData.facebookExpiresIn = facebookAuthRes.expiresIn;
            throw new Error('TODO: Port this functionality to SuperApp Engine');
          }
        );
      }
    );
  }

  public logInWithMagicKey(loginData: any): Observable<sae.IAuthUser> {
    // console.log('logInWithMagicKey(loginData)', loginData);
    return Observable.fromPromise(this.data.getPushToken()).mergeMap(
      (pushToken) => {
        // console.log('pushToken', pushToken);
        loginData.pushToken = pushToken;
        loginData.platform = this.device.platform;
        loginData.deviceid = this.device.uuid;
        loginData.version = config.appVersion;
        throw new Error('TODO: Port this functionality to SuperApp Engine');
      }
    );
  }

  private resolveFacebookLogin(): Observable<any> {
    // console.log('resolveFacebookLogin()');

    // https://ionicframework.com/docs/native/facebook/
    return Observable.fromPromise(this.facebook.getLoginStatus()).mergeMap(
      (fbLoginRes: FacebookLoginResponse): Observable<any> => {
        // console.log('fbLoginRes', fbLoginRes);
        if (fbLoginRes.status === 'connected') {
          return Observable.of(fbLoginRes);
        } else {
          return Observable.fromPromise(this.facebook.login(config.facebookPermissions))
        }
      }
    ).mergeMap(
      (fbLoginRes: FacebookLoginResponse) => {
        // console.log('fbLoginRes', fbLoginRes);
        if (fbLoginRes.status === 'connected') {
          return Observable.of(fbLoginRes.authResponse);
        }
      }
    );
  }

  public registerAsGuest(regData: any): Observable<sae.IAuthUser> {
    // console.log('registerAsGuest(regData)', regData);

    regData.is_guest = 1;
    if (regData.terms_accepted_at === 'now') {
      // 2015-10-26T07:46:36.611Z -> 2015-10-26T07:46:36
      regData.terms_accepted_at = new Date().toJSON().substr(0, 19);
    }
    regData.with_attributes = 1;

    return this.superapp.Auth.register(regData).do(
      (user) => this.createPushToken(user.token)
    );
  }

  public logOut(): Observable<void> {
    // console.log('logOut()');

    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);

        // clear storage regardles of presence of auth token
        this.data.clearStorage();

        if (!authToken || !authToken.length) {
          return Observable.empty();
        }

        return this.superapp.setToken(authToken).Auth.logout();
      }
    );
  }

  public updateUser(userData: sae.IAuthUpdateSpecs): Observable<sae.IAuthUser> {
    // console.log('updateUser(userData)', userData);
    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);
        // userData.version = config.appVersion;
        if (userData.terms_accepted_at === 'now') {
          // 2015-10-26T07:46:36.611Z -> 2015-10-26T07:46:36
          userData.terms_accepted_at = new Date().toJSON().substr(0, 19);
        }

        if (!userData.terms_accepted_at) {
          delete userData.terms_accepted_at;
        }
        if (!userData.timezone) {
          delete userData.timezone;
        }

        return this.superapp.setToken(authToken).Auth.update(userData).mergeMap(
          // workaroud - we need authentication token, groups and attributes
          () => this.user().do(
            // Push token might have changed since login -
            // for now update every time.
            // Later - remember the commited push token
            () => this.createPushToken(authToken)
          )
        );
      }
    );
  }

  public exportData(): Observable<any> {
    // console.log('exportData()');

    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);

        if (!authToken || !authToken.length) {
          return Observable.empty();
        }

        return this.superapp.setToken(authToken).Auth.download();
      }
    );
  }

  public deleteUser(): Observable<any> {
    // console.log('deleteUser()');

    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);

        if (!authToken || !authToken.length) {
          return Observable.empty();
        }

        return this.superapp.setToken(authToken).Auth.request_deletion();
      }
    );
  }

  public joinGroup(groupId: number): Observable<void> {
    // console.log('joinGroup(groupId)', groupId);
    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);
        return this.superapp.setToken(authToken).Auth.subscribe({ groups: String(groupId) });
      }
    );
  }

  public recognize(recognizeQue: any): Observable<void> {
    // console.log('recognize(recognizeQue)', recognizeQue);
    return this.superapp.Auth.reminder(recognizeQue);
  }

  public verify(verifyQue: any): Observable<sae.IAuthUser> {
    // console.log('recognize(verifyQue)', verifyQue);
    let authLoginParams: sae.IAuthLoginParams = {
      with_attributes: 1,
    };
    authLoginParams[verifyQue.channel] = verifyQue.auth_field;

    // Apple review special case - don't send request to regenerate verification
    if (verifyQue.auth_field.includes('+AppleReview')) {
      authLoginParams.password = verifyQue.verification;
    } else {
      authLoginParams.pin = verifyQue.verification;
    }

    return this.superapp.Auth.login(authLoginParams).do(
      (user) => this.createPushToken(user.token)
    );
  }

  private async createPushToken(authToken: string): Promise<sae.IPushToken | void> {
    // console.log('createPushToken(authToken)', authToken);
    var pushToken: string;
    await this.storage.get('appsup/pushToken').then(
      (pt) => pushToken = pt
    );
    // console.log('pushToken', pushToken);
    if (!pushToken) {
      console.warn('Push token not acquired');
      return Promise.resolve();
    }
    var pushReg: sae.IPushTokenSpecs = {
      push_token: pushToken,
      device_uid: this.device.uuid,
      app_version: config.appVersion,
    };

    let platform = this.determinePlatform();
    if (platform === 'android' || platform === 'ios') {
      pushReg.platform = platform;
    } else {
      console.error('Unsuported platform for push notifications');
    }

    return this.superapp.setToken(authToken).PushToken.create(pushReg).toPromise();
  }

  private determinePlatform(): string {
    // console.log('determinePlatform()');
    let platform = this.device.platform;
    if (!this.device.platform) {
      return '';
    }
    return platform.toLowerCase();
  }

}
