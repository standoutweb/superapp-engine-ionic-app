import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';
import * as saeguards from 'superapp-engine-api-typescript-client/dist/common/type.guards';

import { config } from '../../config';
import { HelpersService } from '../../services/helpers/helpers.service';
import { SuperappEngineApiService } from '../../services/superapp-engine-api/superapp-engine-api.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public state: any = {};

  private sections = {
    data: new BehaviorSubject<Array<sae.IDestination>>([]),
    errors: new BehaviorSubject<Array<any>>([]),
    fetching: false,
  }

  private qSets = {
    observer: new BehaviorSubject<Array<sae.ICategory>>(null),
    fetching: false,
  }

  private answerCountInCat: { answered_questions: number; unanswered_questions: number }[] = [];

  private messages = {
    observer: new BehaviorSubject<Array<sae.IMessageLike> | false>(null),
    fetching: false,
  }

  private catalogs = {
    observer: new BehaviorSubject<Array<sae.IGroupPlugin> | false>(null),
    fetching: false,
  }

  private categories = [];

  private articles = [];

  private tickets = [];

  private deposits = [];

  constructor(
    public events: Events,
    public storage: Storage,
    public superapp: SuperappEngineApiService,
    public helpers: HelpersService,
  ) {
    // console.log('DataService::constructor()');
  }

  public clearObservers() {
    // console.log('clearObservers()');
    // TODO: something smarter here
    this.sections.data.next([]);

    this.clearQuestionaries();
    this.messages.observer.next(null);
    this.categories = [];
    this.articles = [];
    this.tickets = [];
    this.deposits = [];
  }

  public clearStorage(): Promise<any> {
    // console.log('clearStorage()');
    this.clearObservers();

    return this.storage.clear();
  }

  public getUser(): Observable<sae.IAuthUser> {
    // console.log('getUser()');
    return Observable.fromPromise(this.storage.get('appsup/user')).map(
      (userStr) => {
        // console.log('userStr', userStr);
        const errmsg = 'User data not stored locally';
        if (!userStr) {
          throw new Error(errmsg);
        }
        let user = JSON.parse(userStr);
        if (!user || !saeguards.isAuthUser(user)) {
          throw new Error(errmsg);
        }
        return user;
      }
    );
  }

  public setUser(user: sae.IAuthUser) {
    // console.log('setUser(user)', user);
    this.storage.set('appsup/user', JSON.stringify(user));
  }

  // TODO: Restructure so that auth token is abstracted away in User API
  public getAuthToken(): Promise<string> {
    // console.log('getAuthToken()');
    return this.storage.get('appsup/authToken');
  }

  // TODO: Restructure so that auth token is abstracted away in User API
  public setAuthToken(authToken: string): Observable<any> {
    // console.log('setAuthToken(authToken)', authToken);
    return Observable.fromPromise(this.storage.set('appsup/authToken', authToken));
  }

  public getPushToken(): Promise<string> {
    // console.log('getPushToken()');
    return this.storage.get('appsup/pushToken');
  }

  public setPushToken(pushToken: string): Observable<any> {
    // console.log('setPushToken(pushToken)', pushToken);
    return Observable.fromPromise(this.storage.set('appsup/pushToken', pushToken));
  }

  public getPublicSetting(key: string): Observable<sae.ISetting> {
    // console.log('getPublicSetting(key: string)', key);
    return this.superapp.Setting.public(`global.${key}`);
  }

  private getMedia(media_id: number): Promise<sae.IMediaImage> {
    // console.log('getMedia(media_id)', media_id);
    return this.storage.get('appsup/authToken').then(
      (authToken: string) => {
        // console.log('authToken', authToken);
        return (this.superapp.setToken(authToken).Media.get(media_id) as Observable<sae.IMediaImage>).toPromise();
      }
    );
  }

  public getActiveGroup(): Promise<sae.IGroup> {
    // console.log('getActiveGroup()');
    return this.storage.get('appsup/activeGroupObject').then(
      (group: sae.IGroup) => {
        if (!saeguards.isGroup(group)) {
          throw new Error('Group not selected');
        }
        return group;
      }
    );
  }

  public setActiveGroup(activeGroup: sae.IGroup) {
    // console.log('setActiveGroup(activeGroup)', activeGroup);
    return this.storage.set('appsup/activeGroupObject', activeGroup);
  }

  public getAppConfig(): Observable<any> {
    // console.log('getAppConfig()');

    let keys = [
      'terms_and_conditions',
      'terms_and_conditions_updated_at',
    ];

    let settingSources = [];

    keys.forEach(key => {
      let obs = this.getPublicSetting(key).map(
        (setting) => [key, setting.value]
      );
      settingSources.push(obs);
    });

    return Observable.zip(...settingSources).map(
      (pairs) => pairs.reduce((obj, pair) => { obj[pair[0]] = pair[1]; return obj; }, {})
    );
  }

  public listGroups(): Observable<Array<sae.IGroup>> {
    // console.log('listGroups()');

    return this.getUser().mergeMap(
      (user) => {
        // console.log('user', user);
        if (!user) {
          return Observable.throw('Not authenticated');
        }
        return this.superapp.setToken(user.token).Group.list({ with_attributes: 1, expand_attributes: 1 }).map(
          (grpCol) => this.transformGroupCollection(grpCol, user)
        );
      }
    );
  }

  public getGroup(groupId: number): Observable<sae.IGroup> {
    // console.log('getGroup(groupId)', groupId);
    return this.getUser().mergeMap(
      (user) => {
        // console.log('user', user);
        if (!user) {
          return Observable.throw('Not authenticated');
        }
        return this.superapp.setToken(user.token).Group.get(groupId, { with_attributes: 1, expand_attributes: 1 }).map(
          (grp) => this.transformGroup(grp, user)
        );
      }
    );
  }

  private transformGroupCollection(grpCol: sae.ICollection<sae.IGroup>, user: sae.IAuthUser): Array<sae.IGroup> {
    // console.log('transformGroupCollection(grpCol, user)', grpCol, user);

    grpCol.items.forEach(
      (grp) => grp = this.transformGroup(grp, user)
    );
    return grpCol.items;
  }

  private transformGroup(grp: sae.IGroup, user: sae.IAuthUser): sae.IGroup {
    // console.log('transformGroup(grp, user)', grp, user);

    if (!grp.attributes._thumbs) grp.attributes._thumbs = {};
    if (!grp.attributes._h500) grp.attributes._h500 = {};

    Object.keys(grp.attributes).forEach(
      (slug) => {
        if (!grp.attributes[slug]) return;
        let media: sae.IMediaImage = grp.attributes[slug];
        if (media.presets) {
          grp.attributes[slug] = media.url;
          grp.attributes._thumbs[slug] = media.presets.thumb;
          grp.attributes._h500[slug] = media.presets.h500;
        }
      }
    );

    (grp as any).is_member = Boolean(user.groups.indexOf(grp.id) >= 0);

    // console.log('grp', grp);
    return grp;
  }

  public getSections(): Observable<Array<sae.IDestination>> {
    // console.log('getSections()');
    // TODO: for now fine. Later use cache and pull-to-refresh
    var val = this.sections.data.getValue();
    // console.log('this.sections.data.getValue()', val);
    // console.log('this.sections.fetching', this.sections.fetching);
    // TODO: implement such filter for other shared observables
    if (!val.length && !this.sections.fetching) {
      this.sections.fetching = true;
      this.pullSections();
    }
    return this.sections.data.asObservable();
  }

  public pullSections() {
    // console.log('pullSections()');
    this.storage.get('appsup/authToken').then(
      (authToken: string) => {
        // console.log('authToken', authToken);
        if (!authToken) {
          this.sections.fetching = false;
          return;
        }
        this.storage.get('appsup/activeGroupObject').then(
          (activeGroup: sae.IGroup) => {
            // console.log('activeGroup', activeGroup);
            if (!activeGroup || activeGroup.id < 1) {
              this.sections.fetching = false;
              return;
            }
            this.superapp.setToken(authToken).Menu.list(activeGroup.id).finally(
              () => this.sections.fetching = false
            ).map(
              (menusCol) => this.transformSections(menusCol)
            ).map(
              (sections: Array<sae.IDestination>) => this.processSections(sections)
            ).subscribe(
              (sections: Array<sae.IDestination>) => {
                // console.log('sections', sections);
                this.sections.data.next(sections);
              },
              (err) => {
                // console.error(err);
                if (err.message == 'Not authorized') {
                  this.events.publish('logout');
                  return;
                }
                this.helpers.handleError(err);
              }
            );
          },
          (err) => console.error(err)
        );
      },
      (err) => console.error(err)
    );
  }

  private transformSections(menusCol: sae.ICollection<sae.IMenu>): sae.IDestination[] {
    // console.log('transformSections(menusCol)', menusCol);
    const menu = menusCol.items.find((menu: sae.IMenu) => menu.enabled && menu.slug === 'ionic-app-sidemenu');
    if (!menu || !menu.destinations) return [];
    return menu.destinations;
  }

  private processSections(sections: Array<sae.IDestination>): Array<sae.IDestination> {
    // console.log('processSections(sections)', sections);
    // build module index
    var moduleIndex = {};
    sections.forEach((section: sae.IDestination) => {
      moduleIndex[section.component] = true;
      section.params = JSON.parse(section.params);
      if (!section.params) section.params = {};
    });
    // add extra sections
    config.localSections.forEach(
      (locSec: sae.IDestination) => {
        // console.log('locSec', locSec);
        // don't insert duplicative items
        if (moduleIndex[locSec.component]) return;
        // console.log('insert unique by module');
        sections.push(locSec);
      }
    );
    // sort by order
    sections.sort((a, b) => a.order - b.order);
    // console.log('sections', sections);
    // filter out unhandled sections
    return sections.filter(
      (section: sae.IDestination) => {
        // console.log('section', section);
        return this.canHandlePlugin(section.component);
      }
    );
  }

  public getQuestionSets(): Observable<Array<sae.ICategory>> {
    // console.log('getQuestionSets()');
    // TODO: for now fine. Later use cache and pull-to-refresh
    var val = this.qSets.observer.getValue();
    // console.log('this.qSets.observer.getValue()', val);
    // console.log('this.qSets.fetching', this.qSets.fetching);
    // TODO: implement such filter for other shared observables
    if (!val && !this.qSets.fetching) {
      this.qSets.fetching = true;
      this.pullQuestionSets();
    }
    return this.qSets.observer.asObservable();
  }

  public pullQuestionSets() {
    // console.log('pullQuestionSets()');
    this.storage.get('appsup/activeGroupObject').then(
      (activeGroup: sae.IGroup) => {
        // console.log('activeGroup', activeGroup);
        if (!activeGroup || activeGroup.id < 1) {
          // this.qSets.observer.next(null);
          this.qSets.fetching = false;
          return;
        }
        this.getUser().subscribe(
          (user: sae.IAuthUser) => {
            // console.log('user', user);
            if (!user.token) {
              this.qSets.fetching = false;
              return;
            }
            let params: sae.ICollectionParams = { per_page: 200 };
            this.superapp.setToken(user.token).UserAttribute.list(activeGroup.id, params).subscribe(
              (attributeCol: sae.ICollection<sae.IAttribute>) => {
                // console.log('attributeCol', attributeCol);
                this.answerCountInCat = [];
                attributeCol.items.forEach(
                  (attr: sae.IAttribute) => {
                    if (!config.handledAttributeTypes[attr.type]) {
                      // console.log('Unhandled attribute type');
                      return;
                    }
                    if (!attr.categories.length) {
                      // console.log('Uncategorized attribute');
                      return;
                    }
                    // TODO: assuming there is single category
                    let catId = attr.categories[0].id;
                    if (!this.answerCountInCat[catId]) this.answerCountInCat[catId] = { answered_questions: 0, unanswered_questions: 0 };
                    if (user.attributes[attr.slug] && String(user.attributes[attr.slug]).length) {
                      this.answerCountInCat[catId].answered_questions++;
                    } else {
                      this.answerCountInCat[catId].unanswered_questions++;
                    }
                  }
                );
                // console.log('this.answerCountInCat', this.answerCountInCat);

                this.superapp.setToken(user.token).UserAttributeCategory.list(activeGroup.id, { with_attributes: 1 }).finally(
                  () => this.qSets.fetching = false
                ).subscribe(
                  (categoryCol: sae.ICollection<sae.ICategory>) => {
                    // console.log('categoryCol', categoryCol);
                    categoryCol.items.forEach(
                      (cat: sae.ICategory) => {
                        if (this.answerCountInCat[cat.id]) {
                          (cat as any).answered_questions = this.answerCountInCat[cat.id].answered_questions;
                          (cat as any).unanswered_questions = this.answerCountInCat[cat.id].unanswered_questions;
                        } else {
                          (cat as any).answered_questions = 0;
                          (cat as any).unanswered_questions = 0;
                        }
                      }
                    );
                    // console.log('categoryCol.items', categoryCol.items);

                    this.qSets.observer.next(categoryCol.items);
                  },
                  // TODO: is error propogated up if not sent on next()?
                  (err) => console.error(err)
                );

              },
              (err) => console.error(err)
            );
          },
          (err) => console.error(err)
        );
      },
      (err) => console.error(err)
    );
  }

  public clearQuestionaries() {
    this.qSets.observer.next(null);
  }

  public getNextQuestion(groupId: number, qSetId: number, newOnly?: boolean, prevQuest?: number): Observable<sae.IAttribute> {
    // console.log('getNextQuestion(groupId, qSetId, newOnly, prevQuest)', [groupId, qSetId, newOnly, prevQuest]);

    // TODO: Can use API request parameter to filter by category
    return this.getUser().mergeMap(
      (user: sae.IAuthUser) => {
        // console.log('user', user);
        let params: sae.ICollectionParams = { per_page: 200 };
        return this.superapp.setToken(user.token).UserAttribute.list(groupId, params).map(
          (attributeCol: sae.ICollection<sae.IAttribute>) => {
            // console.log('attributeCol', attributeCol);
            let nextQuest = attributeCol.items.find(
              (attribute) => {
                // console.log('attribute', attribute);
                if (!config.handledAttributeTypes[attribute.type]) {
                  // console.log('Unhandled attribute type');
                  return false;
                }
                if (!attribute.categories.length) {
                  // console.log('Uncategorized attribute');
                  return false;
                }
                // TODO: assuming there is single category
                if (attribute.categories[0].id != qSetId) {
                  // console.log('Incorrect category');
                  return false;
                }
                if (newOnly && user.attributes[attribute.slug] && String(user.attributes[attribute.slug]).length) {
                  // console.log('Already answered');
                  return false;
                }
                if (prevQuest && attribute.id <= prevQuest) {
                  // console.log('Some of the previous attributes');
                  return false;
                }
                return true;
              }
            );
            if (nextQuest) {
              // console.log('user.attributes[nextQuest.slug]', user.attributes[nextQuest.slug]);
              let answers = user.attributes[nextQuest.slug];
              if (!answers) {
                answers = nextQuest.is_collection ? [] : null;
              }
              (nextQuest as any).answers = answers;
            }
            // console.log('nextQuest', nextQuest);
            return nextQuest;
          }
        );
      }
    );
  }

  public getMessages(): Observable<Array<sae.IMessageLike> | false> {
    // console.log('getMessages()');
    // TODO: for now fine. Later use cache and pull-to-refresh
    var val = this.messages.observer.getValue();
    // console.log('this.messages.observer.getValue()', val);
    // console.log('this.messages.fetching', this.messages.fetching);
    // TODO: implement such filter for other shared observables
    if (!val && !this.messages.fetching) {
      this.messages.fetching = true;
      this.pullMessages();
    }
    return this.messages.observer.asObservable();
  }

  public pullMessages() {
    // console.log('pullMessages()');
    this.storage.get('appsup/authToken').then(
      (authToken: string) => {
        // console.log('authToken', authToken);
        if (!authToken) {
          this.messages.fetching = false;
          return;
        }
        this.storage.get('appsup/activeGroupObject').then(
          (activeGroup: sae.IGroup) => {
            // console.log('activeGroup', activeGroup);
            if (!activeGroup || activeGroup.id < 1) {
              this.messages.fetching = false;
              return;
            }

            this.superapp.setToken(authToken).AuthMessage.list({ type: 'push', groups: String(activeGroup.id), sort: 'id:desc' }).finally(
              () => this.messages.fetching = false
            ).catch(
              (err) => {
                // console.log('err', err);
                // TODO: how would we handle non-Http errors
                this.messages.observer.next(false);
                this.helpers.handleError(err);
                // we have to return but has no other purpose for us
                return Observable.throw(err);
              }
            ).map(
              (msgCol: sae.ICollection<sae.IMessageLike>) => {
                msgCol.items.forEach(
                  (msg) => {
                    msg.template_destinations.forEach(
                      (dest) => dest.params = JSON.parse(dest.params)
                    );

                    if (!msg.template_attributes.message_image) return;
                    this.getMedia(Number(msg.template_attributes.message_image)).then(
                      (media: sae.IMediaImage) => {
                        (msg as any).image = media.url;
                        (msg as any).image_thumb = media.presets.thumb;
                      }
                    );
                  }
                );
                return msgCol.items;
              }
            ).subscribe(
              (messages: Array<sae.IMessageLike>) => {
                // console.log('messages', messages);
                this.messages.observer.next(messages);
              }
            );
          },
          (err) => console.error(err)
        );
      },
      (err) => console.error(err)
    );
  }

  public getMessage(messageId: number): Observable<sae.IMessageLike> {
    // console.log('getMessage(messageId)', messageId);

    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);
        return Observable.fromPromise(this.storage.get('appsup/activeGroupObject')).mergeMap(
          (activeGroup: sae.IGroup) => {
            // console.log('activeGroup', activeGroup);
            return this.superapp.setToken(authToken).AuthMessage.list({ groups: String(activeGroup.id) }).map(
              (messages) => messages.items.find((mes) => messageId == mes.id)
            );
          }
        );
      }
    );
  }

  public markMessageSeen(messageId: number): Observable<sae.IMessageLike> {
    // console.log('markMessageSeen(messageId)', messageId);
    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);
        return this.superapp.setToken(authToken).AuthMessage.touch(messageId);
      }
    );
  }

  public listCatalogs(): Observable<Array<sae.IGroupPlugin> | false> {
    // console.log('listCatalogs()');
    // TODO: for now fine. Later use cache and pull-to-refresh
    var val = this.catalogs.observer.getValue();
    // console.log('this.catalogs.observer.getValue()', val);
    // console.log('this.catalogs.fetching', this.catalogs.fetching);
    // TODO: implement such filter for other shared observables
    if (!val && !this.catalogs.fetching) {
      this.catalogs.fetching = true;
      this.pullCatalogs();
    }
    return this.catalogs.observer.asObservable();
  }

  public pullCatalogs() {
    // console.log('pullCatalogs()');
    this.storage.get('appsup/authToken').then(
      (authToken: string) => {
        // console.log('authToken', authToken);
        if (!authToken) {
          this.catalogs.fetching = false;
          return;
        }
        this.storage.get('appsup/activeGroupObject').then(
          (activeGroup: sae.IGroup) => {
            // console.log('activeGroup', activeGroup);
            if (!activeGroup || activeGroup.id < 1) {
              this.catalogs.fetching = false;
              return;
            }
            this.superapp.setToken(authToken).GroupPlugin.list(activeGroup.id).finally(
              () => this.catalogs.fetching = false
            ).catch(
              (err) => {
                // console.log('err', err);
                // TODO: how would we handle non-Http errors
                this.catalogs.observer.next(false);
                this.helpers.handleError(err);
                // we have to return but has no other purpose for us
                return Observable.throw(err);
              }
            ).subscribe(
              (catalogCol: sae.ICollection<sae.IGroupPlugin>) => {
                // console.log('catalogCol', catalogCol);
                this.catalogs.observer.next(catalogCol.items);
              }
            );
          },
          (err) => console.error(err)
        );
      },
      (err) => console.error(err)
    );
  }

  public listCategories(catalogId: number, category?: number): Observable<Array<sae.ICategory>> {
    // console.log('listCategories()');

    var catAndCat = catalogId + '_' + category;

    if (!this.categories[catAndCat]) {
      this.categories[catAndCat] = {
        observer: new BehaviorSubject<Array<sae.ICategory> | null>(null),
        fetching: false,
      }
    }

    // TODO: for now fine. Later use cache and pull-to-refresh
    var val = this.categories[catAndCat].observer.getValue();
    // TODO: implement such filter for other shared observables
    if (!val && !this.categories[catAndCat].fetching) {
      this.categories[catAndCat].fetching = true;
      this.pullCategories(catalogId, category);
    }
    return this.categories[catAndCat].observer.asObservable();
  }

  public pullCategories(catalogId: number, category?: number) {
    // console.log('pullCategories()');

    var catAndCat = catalogId + '_' + category;

    this.storage.get('appsup/activeGroupObject').then(
      (activeGroup: sae.IGroup) => {
        // console.log('activeGroup', activeGroup);
        Observable.from([{ todo: 'Port this functionality to SuperApp Engine' }]).finally(
          () => this.categories[catAndCat].fetching = false
        ).catch(
          (err) => {
            // console.log('err', err);
            // TODO: how would we handle non-Http errors
            this.categories[catAndCat].observer.next(null);
            this.helpers.handleError(err);
            // we have to return but has no other purpose for us
            return Observable.throw(err);
          }
        ).subscribe(
          (categories) => {
            // console.log('categories', categories);
            this.categories[catAndCat].observer.next(categories);
          }
        );
      },
      (err) => console.error(err)
    );
  }

  public listArticles(catalogId: number, category?: number, orderBy?: string): Observable<Array<sae.IArticle>> {
    // console.log('listArticles()');

    // TODO: Suport multiple categories?

    var catAndCat = catalogId + '_' + category;

    if (!this.articles[catAndCat]) {
      this.articles[catAndCat] = {
        observer: new BehaviorSubject<Array<sae.IArticle> | null>(null),
        fetching: false,
      }
    }

    // TODO: for now fine. Later use cache and pull-to-refresh
    var val = this.articles[catAndCat].observer.getValue();
    // TODO: implement such filter for other shared observables
    if (!val && !this.articles[catAndCat].fetching) {
      this.articles[catAndCat].fetching = true;
      this.pullArticles(catalogId, category, orderBy);
    }
    return this.articles[catAndCat].observer.asObservable();
  }

  public pullArticles(catalogId: number, category?: number, orderBy?: string) {
    // console.log('pullArticles()');

    var catAndCat = catalogId + '_' + category;
    this.storage.get('appsup/authToken').then(
      (authToken: string) => {
        // console.log('authToken', authToken);
        this.storage.get('appsup/activeGroupObject').then(
          (activeGroup: sae.IGroup) => {
            // console.log('activeGroup', activeGroup);
            let articleListParams: sae.IArticleListParams = {
              group_plugin_id: catalogId,
              with_attributes: 1,
            }
            if (category) articleListParams.categories = String(category);
            if (orderBy) articleListParams.sort = orderBy;

            this.superapp.setToken(authToken).Article.list(activeGroup.id, articleListParams).finally(
              () => this.articles[catAndCat].fetching = false
            ).catch(
              (err) => {
                // console.log('err', err);
                // TODO: how would we handle non-Http errors
                this.articles[catAndCat].observer.next(null);
                this.helpers.handleError(err);
                // we have to return but has no other purpose for us
                return Observable.throw(err);
              }
            ).map(
              (artCol) => this.transformArticleCollection(artCol)
            ).subscribe(
              (articles: Array<sae.IArticle>) => {
                // console.log('articles', articles);
                this.articles[catAndCat].observer.next(articles);
              }
            );
          },
          (err) => console.error(err)
        );
      },
      (err) => console.error(err)
    );
  }

  public getArticle(article_id: number): Observable<sae.IArticle> {
    // console.log('getArticle(article_id)', article_id);

    return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
      (authToken: string) => {
        // console.log('authToken', authToken);
        return Observable.fromPromise(this.storage.get('appsup/activeGroupObject')).mergeMap(
          (activeGroup: sae.IGroup) => {
            // console.log('activeGroup', activeGroup);
            return this.superapp.setToken(authToken).Article.get(activeGroup.id, article_id, { with_attributes: 1 }).map(
              (art) => this.transformArticle(art)
            );
          }
        );
      }
    );
  }

  private transformArticleCollection(artCol: sae.ICollection<sae.IArticle>): Array<sae.IArticle> {
    // console.log('transformArticleCollection(artCol)', artCol);
    artCol.items.forEach(
      (art) => art = this.transformArticle(art)
    );
    return artCol.items;
  }

  private transformArticle(art: sae.IArticle) {
    // console.log('transformArticle(art)', art);
    if (!art.attributes.article_image) {
      (art as any).images = [];
      return art;
    }
    this.getMedia(Number(art.attributes.article_image)).then(
      (media: sae.IMediaImage) => (art as any).images = [media.url]
    );

    return art;
  }

  public listTickets(group_plugin_id: number): Observable<Array<sae.ITicket> | false> {
    // console.log('listTickets(group_plugin_id)', group_plugin_id);
    if (!this.tickets[group_plugin_id]) {
      this.tickets[group_plugin_id] = {
        observer: new BehaviorSubject<Array<sae.ITicket> | false>(null),
        fetching: false,
      }
    }

    // TODO: for now fine. Later use cache and pull-to-refresh
    var val = this.tickets[group_plugin_id].observer.getValue();
    // console.log('this.tickets[group_plugin_id].observer.getValue()', val);
    // console.log('this.tickets[group_plugin_id].fetching', this.tickets[group_plugin_id].fetching);
    // TODO: implement such filter for other shared observables
    if (!val && !this.tickets[group_plugin_id].fetching) {
      this.tickets[group_plugin_id].fetching = true;
      this.pullTickets(group_plugin_id);
    }
    return this.tickets[group_plugin_id].observer.asObservable();
  }

  public pullTickets(group_plugin_id: number) {
    // console.log('pullTickets(group_plugin_id)', group_plugin_id);
    this.storage.get('appsup/authToken').then(
      (authToken: string) => {
        // console.log('authToken', authToken);
        let params: sae.IAuthTicketListParams = {
          group_plugin_id: group_plugin_id,
        };
        this.superapp.setToken(authToken).Ticket.authList(params).finally(
          () => this.tickets[group_plugin_id].fetching = false
        ).catch(
          (err) => {
            // console.log('err', err);
            // TODO: how would we handle non-Http errors
            this.tickets[group_plugin_id].observer.next(false);
            this.helpers.handleError(err);
            // we have to return but has no other purpose for us
            return Observable.throw(err);
          }
        ).subscribe(
          (ticketCol: sae.ICollection<sae.ITicket>) => {
            // console.log('ticketCol', ticketCol);
            this.tickets[group_plugin_id].observer.next(ticketCol.items);
          }
        );
      },
      (err) => console.error(err)
    );
  }

  public listDeposits(group_plugin_id: number): Observable<Array<sae.IDeposit> | false> {
    // console.log('listDeposits(group_plugin_id)', group_plugin_id);
    if (!this.deposits[group_plugin_id]) {
      this.deposits[group_plugin_id] = {
        observer: new BehaviorSubject<Array<sae.IDeposit> | false>(null),
        fetching: false,
      }
    }

    // TODO: for now fine. Later use cache and pull-to-refresh
    var val = this.deposits[group_plugin_id].observer.getValue();
    // console.log('this.deposits[group_plugin_id].observer.getValue()', val);
    // console.log('this.deposits[group_plugin_id].fetching', this.deposits[group_plugin_id].fetching);
    // TODO: implement such filter for other shared observables
    if (!val && !this.deposits[group_plugin_id].fetching) {
      this.deposits[group_plugin_id].fetching = true;
      this.pullDeposits(group_plugin_id);
    }
    return this.deposits[group_plugin_id].observer.asObservable();
  }

  public pullDeposits(group_plugin_id: number) {
    // console.log('pullDeposits(group_plugin_id)', group_plugin_id);
    this.storage.get('appsup/authToken').then(
      (authToken: string) => {
        // console.log('authToken', authToken);
        let params: sae.IAuthDepositListParams = {
          group_plugin_id,
          with_attributes: 1,
        };
        this.superapp.setToken(authToken).Deposit.authList(params).finally(
          () => this.deposits[group_plugin_id].fetching = false
        ).catch(
          (err) => {
            // console.log('err', err);
            // TODO: how would we handle non-Http errors
            this.deposits[group_plugin_id].observer.next(false);
            this.helpers.handleError(err);
            // we have to return but has no other purpose for us
            return Observable.throw(err);
          }
        ).subscribe(
          (depositCol: sae.ICollection<sae.IDeposit>) => {
            // console.log('depositCol', depositCol);
            this.deposits[group_plugin_id].observer.next(depositCol.items);
          }
        );
      },
      (err) => console.error(err)
    );
  }

  public listDepositCategories(group_plugin_id: number): Observable<Array<sae.ICategory>> {
    // console.log('listDepositCategories(group_plugin_id)', group_plugin_id);
    return Observable.fromPromise(this.storage.get('appsup/activeGroupObject')).mergeMap(
      (activeGroup: sae.IGroup) => {
        // console.log('activeGroup', activeGroup);
        return Observable.fromPromise(this.storage.get('appsup/authToken')).mergeMap(
          (authToken: string) => {
            // console.log('authToken', authToken);
            let params: sae.IDepositCategoryListParams = {
              group_plugin_id,
              with_attributes: 1,
            };
            return this.superapp.setToken(authToken).DepositCategory.list(activeGroup.id, params).map(
              (catCol: sae.ICollection<sae.ICategory>) => {
                return catCol.items;
              }
            );
          }
        );
      }
    );
  }

  private canHandlePlugin(pluginAlias: string): boolean {
    // console.log('canHandlePlugin(pluginAlias)', pluginAlias);

    if (Boolean(config.handledPlugins[pluginAlias])) {
      // console.log('pluginAlias', pluginAlias);
      return true;
    } else {
      console.error('Unhandled component', pluginAlias);
      return false;
    }
  }

  public getQuestionaryReport(groupId: number, user_attribute_category_id: number): Observable<Array<sae.IAttribute>> {
    // console.log('getQuestionaryReport(groupId, user_attribute_category_id)', groupId, user_attribute_category_id);
    return this.getUser().mergeMap(
      (user) => {
        // console.log('user', user);
        return this.superapp.setToken(user.token).UserAttribute.list(groupId, { per_page: 100 }).map(
          (attrCol: sae.ICollection<sae.IAttribute>) => {
            var attrs = attrCol.items.filter(
              (attribute) => {
                // console.log('attribute', attribute);
                if (!config.handledAttributeTypesDisplay[attribute.type]) {
                  // console.log('Unhandled attribute type', attribute.type);
                  return false;
                }
                if (!attribute.categories.length) {
                  // console.log('Uncategorized attribute');
                  return false;
                }
                // TODO: assuming there is single category
                if (attribute.categories[0].id != user_attribute_category_id) {
                  // console.log('Incorrect category');
                  return false;
                }

                return true;
              }
            ).map(
              (attribute) => {

                (attribute as any).answers = user.attributes[attribute.slug];

                return attribute;
              }
            );
            return attrs;
          }
        );
      }
    );
  }
}
