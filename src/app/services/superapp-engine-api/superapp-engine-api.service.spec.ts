import { TestBed } from '@angular/core/testing';

import { SuperappEngineApiService } from './superapp-engine-api.service';

describe('SuperappEngineApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuperappEngineApiService = TestBed.get(SuperappEngineApiService);
    expect(service).toBeTruthy();
  });
});
