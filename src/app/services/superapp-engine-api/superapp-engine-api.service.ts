import { Injectable } from '@angular/core';

import { Device } from '@ionic-native/device/ngx';

import { config } from '../../config';
import { SuperappEngine } from 'superapp-engine-api-typescript-client';

@Injectable({
  providedIn: 'root'
})
export class SuperappEngineApiService extends SuperappEngine {

  constructor(
    private device: Device,
  ) {
    // console.log('SuperappEngineApiService::constructor()');

    super(config.saeServer);

    this.http.interceptors.request.use(
      (requestConfig) => {
        // console.log('requestConfig', requestConfig);
        if (this.device.uuid) {
          requestConfig.headers.common['deviceuid'] = this.device.uuid;
        }
        return requestConfig;
      }
    );
  }

  public setToken(token: string) {
    // console.log('setToken(token: string)', token);
    this.addHeaders({ Authorization: 'Bearer ' + token });
    return this;
  }

}
