import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController, } from '@ionic/angular';
// ionic/core/dist/types/components/spinner/
import { SpinnerTypes } from '@ionic/core/dist/types/components/spinner/spinner-interface';

import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  private loading: HTMLIonLoadingElement;
  private loadingPresented: boolean = false;
  private loadingPresentTimer;
  private loadingDismissTimer;

  constructor(
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private translate: TranslateService,
  ) {
    // console.log('HelpersService::constructor()');
  }

  async presentAlert(title: string, subTitle: string, buttonLabel: string) {
    let alertOpts = {
      title: title,
      subTitle: subTitle,
      buttons: [buttonLabel],
      enableBackdropDismiss: false,
    };
    const alert = await this.alertCtrl.create(alertOpts);
    await alert.present();
  }

  public timeDismissLoading() {
    // console.log('timeDismissLoading()');
    if (this.loadingPresented) {
      clearTimeout(this.loadingDismissTimer);
      this.loadingDismissTimer = null;
      // console.log('this.loadingDismissTimer', this.loadingDismissTimer);
      this.loadingDismissTimer = setTimeout(
        () => this.dismissLoading(),
        300
      );
      return;
    }
    if (this.loadingPresentTimer) {
      clearTimeout(this.loadingPresentTimer);
      this.loadingPresentTimer = null;
      // console.log('this.loadingPresentTimer', this.loadingPresentTimer);
    }
  }

  public timePresentLoading(message: string, spinner?: SpinnerTypes) {
    // console.log('timePresentLoading(message)', message);
    // console.log('this.loadingPresentTimer', this.loadingPresentTimer);
    if (this.loadingPresentTimer) {
      // console.log('already going to present; exiting');
      return;
    }
    if (this.loadingPresented) {
      // console.log('going to extend');
      clearTimeout(this.loadingDismissTimer);
      this.loadingDismissTimer = null;
      // console.log('this.loadingDismissTimer', this.loadingDismissTimer);
    } else {
      this.loadingPresentTimer = setTimeout(
        () => this.presentLoading(message, spinner),
        300
      );
    }
    // make sure it's dismissed at the end no matter what
    this.loadingDismissTimer = setTimeout(
      () => this.dismissLoading(),
      5000
    );
  }

  public dismissLoading() {
    // console.log('dismissLoading()');
    // console.trace();
    this.loadingDismissTimer = null;
    if (this.loadingPresented) {
      this.loadingPresented = false;
      this.loading.dismiss();
    }
    // TODO: Won't we have race condition here?
  }

  public async presentLoading(message?: string, spinner?: SpinnerTypes) {
    // console.log('presentLoading(message)', message);
    // console.trace();
    this.loadingPresentTimer = null;
    this.loading = await this.loadingCtrl.create({
      spinner: (spinner ? spinner : 'dots'),
      message: message,
      cssClass: (message.length ? 'has-text' : '')
      // duration: 5000,
    });
    this.loadingPresented = true;
    await this.loading.present();
  }

  public async handleError(error) {
    console.error('handleError(error)', error);
    // console.trace();
    var message = 'An unspecified error occured';
    if (typeof error === 'string') {
      message = error;
    }
    if (message == 'cordova_not_available') {
      message = this.translate.instant('This function is not availabe');
    }
    if (error.type) {
      message = this.translate.instant('Error. No internet?');
    }
    if (error.response && error.response.data && error.response.data.error) {
      error = error.response.data.error;
    }
    if (error.message) {
      message = error.message;
    }
    if (message.includes('users_email_app_id_unique')) {
      message = this.translate.instant('Error. User exists.');
    }
    if (error.errorMessage) {
      message = error.errorMessage;
    }
    if (error.status_code == 1002) {
      message = this.translate.instant('Invalid phone number or email');
    }
    if (error.status_code == 1003) {
      message = this.translate.instant('Invalid phone number or email');
    }
    if (error.status_code == 1004) {
      message = this.translate.instant('User not found');
    }
    if (error.status_code == 1007) {
      message = this.translate.instant('Verification is invalid');
    }
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
    });
    await toast.present();
  }

}
