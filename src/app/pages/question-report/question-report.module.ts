import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TranslateModule } from '@ngx-translate/core';

import { PipesModule } from '../../pipes/pipes.module';
import { QuestionReportPage } from './question-report.page';

const routes: Routes = [
  {
    path: '',
    component: QuestionReportPage
  }
];

@NgModule({
  declarations: [QuestionReportPage],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),

    IonicModule,

    TranslateModule,

    PipesModule,
  ],
})
export class QuestionReportPageModule { }
