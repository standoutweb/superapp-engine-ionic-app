import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionReportPage } from './question-report.page';

describe('QuestionReportPage', () => {
  let component: QuestionReportPage;
  let fixture: ComponentFixture<QuestionReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionReportPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
