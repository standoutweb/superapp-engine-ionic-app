import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, } from '@ionic/angular';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-question-report',
  templateUrl: './question-report.page.html',
  styleUrls: ['./question-report.page.scss'],
})
export class QuestionReportPage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};

  optionIndexes: Array<any> = [];

  private questions: Array<any>;
  private user: any = {};

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,

    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('QuestionReportPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        if (!sections.length) return;
        this.sections = sections;
      }
    );

    this.events.subscribe('user:loaded', user => {
      // console.log('user', user);
      this.user = user;
    });

    this.data.getUser().subscribe(
      (user: sae.IAuthUser) => {
        this.events.publish('user:loaded', user);
      },
      (err) => console.error(err)
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secid = Number(this.route.snapshot.paramMap.get('secid'));
    if (!secid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (!this.section || this.section.id != secid) {
      this.section = this.sections.find((sec) => sec.id == secid);
      // console.log('section', this.section);
    }

    if (!this.section) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = this.section.id;

    //

    this.getQuestionaryReport();
  }

  getQuestionaryReport() {

    if (!this.group) return;
    if (!this.section) return;

    this.data.getQuestionaryReport(this.group.id, this.section.params.user_attribute_category_id)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (questions: Array<sae.IAttribute>) => {
          // console.log('questions', questions);

          var q = questions.map(
            (question: sae.IAttribute, index) => {
              var next = index + 1;
              // console.log('question', question);

              if (question.type == 'boolean' && !questions[next]) {
                return false;
              }
              if (question.type != 'boolean' && !((question as any).answers && String((question as any).answers))) {
                return false;
              }
              if (question.type == 'boolean' && questions[next].type == 'boolean') {
                return false;
              }
              return question;
            }
          );

          this.questions = q;
        },
        (err) => console.error(err)
      );
    this.helpers.timePresentLoading('');
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    return scripts +
      '<style>' + css + '</style>'
      ;
  }

  getMatching(answers, options) {
    var t = this;
    // console.log('answers', answers);
    answers.filter(function (o1) {
      return options.some(function (o2, index) {
        if (o1 === o2.id) {
          if (t.optionIndexes.indexOf(index) == -1) {
            t.optionIndexes.push(index);
            return true;
          }
        }
      });
    });
  }
}
