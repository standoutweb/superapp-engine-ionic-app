import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, } from '@ionic/angular';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};

  private message: sae.IMessageLike;

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,

    public data: DataService,
  ) {
    // console.log('MessagePage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        if (!sections.length) return;
        this.sections = sections;
      }
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secid = Number(this.route.snapshot.paramMap.get('secid'));
    if (!secid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (!this.section || this.section.id != secid) {
      this.section = this.sections.find((sec) => sec.id == secid);
      // console.log('section', this.section);
    }

    if (!this.section) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = this.section.id;

    //

    var mesid = Number(this.route.snapshot.paramMap.get('mesid'));
    if (!mesid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (this.message && this.message.id == mesid) {
      // same message already set - all good
      return;
    }

    // else load the message
    this.data.getMessage(mesid).subscribe(
      (message) => {
        // console.log('message', message);
        this.message = message;

        // Mark as seen
        if (!this.message.last_read) {
          this.data.markMessageSeen(this.message.id).subscribe(
            () => this.message.last_read = new Date().toJSON().substr(0, 19)
          );
        }
      }
    );
  }

  openSection(sectionConf: sae.IDestination) {
    // console.log('openSection(sectionConf)', sectionConf);
    this.events.publish('section:open', sectionConf);
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    return scripts +
      '<style>' + css + '</style>'
      ;
  }
}
