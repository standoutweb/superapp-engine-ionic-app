import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, } from '@ionic/angular';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.page.html',
  styleUrls: ['./message-list.page.scss'],
})
export class MessageListPage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};

  isLoaded: boolean = false;

  private auMessages: Array<sae.IMessageLike> = [];

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,

    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('MessageListPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.isLoaded = false;
    this.helpers.timePresentLoading('');
    this.data.getMessages().subscribe(
      (auMessages: Array<sae.IMessageLike>) => {
        // console.log('auMessages', auMessages);
        if (auMessages === null) return;
        if (auMessages) {
          this.auMessages = auMessages.filter((msg) => !msg.template_attributes.is_hidden);
          // console.log('this.auMessages', this.auMessages);
        }
        this.isLoaded = true;
        this.helpers.timeDismissLoading();
      }
    );

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        if (!sections.length) return;
        this.sections = sections;
      }
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secid = Number(this.route.snapshot.paramMap.get('secid'));
    if (!secid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (!this.section || this.section.id != secid) {
      this.section = this.sections.find((sec) => sec.id == secid);
      // console.log('section', this.section);
    }

    if (!this.section) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = this.section.id;
  }

  openMessage(message: sae.IMessageLike) {
    // console.log('openMessage(message)', message);

    message.last_read = new Date().toJSON().substr(0, 19);

    this.navCtrl.navigateForward('/message/' + this.section.id + '/' + message.id);
  }

  openDashboardSection() {
    // console.log('openDashboardSection()');
    this.events.publish('section:landing');
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    return scripts +
      '<style>' + css + '</style>'
      ;
  }

  doRefresh(event) {
    // console.log('doRefresh(event)', event);
    this.isLoaded = false;
    this.helpers.timePresentLoading('');
    event.target.complete();

    this.data.pullMessages();
  }

}
