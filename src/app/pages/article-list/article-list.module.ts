import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';

import { PipesModule } from '../../pipes/pipes.module';
import { ArticleListPage } from './article-list.page';

const routes: Routes = [
  {
    path: '',
    component: ArticleListPage
  }
];

@NgModule({
  declarations: [ArticleListPage],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),

    IonicModule,

    FontAwesomeModule,
    TranslateModule,

    PipesModule,
  ],
})
export class ArticleListPageModule { }
