import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, } from '@ionic/angular';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.page.html',
  styleUrls: ['./article-list.page.scss'],
})
export class ArticleListPage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};
  dateFormat: any;

  isLoaded: boolean = false;

  private group_plugin_id: number;
  private category_id: number;
  private articleList: Array<sae.IArticle> = [];

  private items_order: string = '';

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,

    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('ArticleListPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
        this.dateFormat = this.setDateFormat(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        if (!sections.length) return;
        this.sections = sections;
      }
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secid = Number(this.route.snapshot.paramMap.get('secid'));
    if (!secid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (!this.section || this.section.id != secid) {
      this.section = this.sections.find((sec) => sec.id == secid);
      // console.log('section', this.section);
    }

    if (!this.section) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = this.section.id;

    //

    var pluginid = Number(this.section.group_plugin_id);
    if (!pluginid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    var catid = Number(this.route.snapshot.paramMap.get('catid'));
    if (!catid && this.section.params.categories) {
      // NOTE: Taking just the first category
      catid = Number(this.section.params.categories.split(',')[0]);
    }
    // console.log('this.category_id', this.category_id);

    this.section.params.catalog_items_list_type = this.normalizeListStyle(this.section.params.catalog_items_list_type);
    var items_order = this.section.params.catalog_items_order;

    if (this.items_order == items_order) {
      if (this.group_plugin_id == pluginid) {
        if (this.category_id == catid) {
          // same article list already set - all good
          return;
        }
      }
    }

    this.items_order = items_order;
    this.group_plugin_id = pluginid;
    this.category_id = catid;

    this.isLoaded = false;
    this.data.listArticles(this.group_plugin_id, this.category_id, this.items_order).subscribe(
      (articleList: Array<sae.IArticle>) => {
        // console.log('articleList', articleList);
        this.helpers.timePresentLoading('');
        if (!articleList) {
          articleList = [];
        } else {
          this.isLoaded = true;
          this.helpers.timeDismissLoading();
        }
        this.articleList = articleList;
      },
      (err) => console.error(err)
    );
  }

  openArticle(article: sae.IArticle) {
    // console.log('openArticle(article)', article);

    this.navCtrl.navigateForward('/article/' + this.section.id + '/' + article.id);
  }

  openDashboardSection() {
    // console.log('openDashboardSection()');
    this.events.publish('section:landing');
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    if (config.article_list_item_title_color) {
      css = css.concat('page-article-list .item h2 {color: ' + config.article_list_item_title_color + '}');
    }

    return scripts +
      '<style>' + css + '</style>'
      ;
  }

  setDateFormat(config) {
    var dateFormat = 'MMM dd, y';

    if (config.date_format) {
      dateFormat = config.date_format;
    }

    return dateFormat;
  }

  doRefresh(event) {
    // console.log('doRefresh(event)', event);
    this.isLoaded = false;
    this.helpers.timePresentLoading('');
    event.target.complete();

    this.data.pullArticles(this.group_plugin_id, this.category_id, this.items_order);
  }

  normalizeListStyle(listStyle: string): string {
    var listStyles = [
      'list',
      'grid',
    ];
    if (listStyles.indexOf(listStyle) >= 0) {
      return listStyle;
    }
    return listStyles[0];
  }

}
