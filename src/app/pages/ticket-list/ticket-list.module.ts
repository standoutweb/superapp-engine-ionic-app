import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Brightness } from '@ionic-native/brightness/ngx';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';

import { PipesModule } from '../../pipes/pipes.module';
import { TicketListPage } from './ticket-list.page';

const routes: Routes = [
  {
    path: '',
    component: TicketListPage
  }
];

@NgModule({
  declarations: [TicketListPage],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),

    IonicModule,

    FontAwesomeModule,
    TranslateModule,

    PipesModule,
  ],
  providers: [
    Brightness,
  ],
})
export class TicketListPageModule { }
