import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, } from '@ionic/angular';

import { Brightness } from '@ionic-native/brightness/ngx';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';
import * as JsBarcode from 'jsbarcode';

import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.page.html',
  styleUrls: ['./ticket-list.page.scss'],
})
export class TicketListPage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};

  isLoaded: boolean = false;

  tickets: Array<sae.ITicket> = [];

  private sysBrightness: number = -1;

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,

    private brightness: Brightness,

    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('TicketListPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        if (!sections.length) return;
        this.sections = sections;
      }
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secid = Number(this.route.snapshot.paramMap.get('secid'));
    if (!secid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (!this.section || this.section.id != secid) {
      this.section = this.sections.find((sec) => sec.id == secid);
      // console.log('section', this.section);
    }

    if (!this.section) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = this.section.id;

    //

    this.isLoaded = false;
    this.helpers.timePresentLoading('');
    this.data.listTickets(this.section.group_plugin_id).subscribe(
      (tickets: Array<sae.ITicket>) => this.processTickets(tickets)
    );
  }

  processTickets(tickets: Array<sae.ITicket>) {
    // console.log('tickets', tickets);
    if (tickets === null) return;
    if (tickets) {
      var canvas = document.createElement('canvas');
      const barcodeOptions = {
        format: 'CODE128',
        displayValue: false,
      };
      tickets.forEach(
        (ticket) => {
          JsBarcode(canvas, ticket.code, barcodeOptions);
          (ticket as any).image = canvas.toDataURL('image/png');
        }
      );
      canvas.remove();
      this.tickets = tickets;
    }
    this.isLoaded = true;
    this.helpers.timeDismissLoading();
  }

  ionViewDidEnter() {
    this.brightness.getBrightness().then(
      (sysBrightness: number) => {
        // console.log(sysBrightness);
        this.sysBrightness = sysBrightness;
        this.brightness.setKeepScreenOn(true);
        this.brightness.setBrightness(1);
      },
      (err) => console.error(err)
    );
  }

  ionViewWillLeave() {
    // set system default brightness
    this.brightness.setKeepScreenOn(false);
    this.brightness.setBrightness(this.sysBrightness);
  }

  openDashboardSection() {
    // console.log('openDashboardSection()');
    this.events.publish('section:landing');
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    return scripts +
      '<style>' + css + '</style>'
      ;
  }

  doRefresh(event) {
    // console.log('doRefresh(event)', event);
    this.isLoaded = false;
    this.helpers.timePresentLoading('');
    event.target.complete();

    this.data.pullTickets(this.section.group_plugin_id);
  }

}
