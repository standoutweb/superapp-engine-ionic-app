import { Component, OnInit, } from '@angular/core';
import { AlertController, Events, ToastController, NavController, } from '@ionic/angular';
import { AlertInput } from '@ionic/core';
import { Observable } from 'rxjs';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';
import { TranslateService } from '@ngx-translate/core';

import { config } from '../../config';
import { AuthService } from '../../services/auth/auth.service';
import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  private appConfig: { [key: string]: string } = {};
  private group: sae.IGroup;

  styles: string = '';
  config: sae.IAttributeValues = {};

  private user: sae.IAuthUser;
  private qSets: Observable<Array<sae.ICategory>>;
  private qSetsLen: number;

  private emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  private ccodeRegex = /^(\+?\d{1,4})$/;
  private phoneRegex = /^(\d{7,13})$/;

  private guestRegisterPrompt: HTMLIonAlertElement;

  constructor(
    public alertCtrl: AlertController,
    public events: Events,
    public navCtrl: NavController,
    public toastCtrl: ToastController,

    public translate: TranslateService,

    public auth: AuthService,
    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('ProfilePage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');
    this.loadAppConfig();

    this.loadUser();

    this.qSets = this.data.getQuestionSets().map(
      (categories) => {
        if (!categories) return [];
        return categories.filter((cat) => !cat.attributes.is_private)
      }
    );

    // count
    this.qSets.subscribe(
      (qSets: Array<sae.ICategory>) => {
        if (!qSets) return;
        this.qSetsLen = qSets.length;
      },
      (err) => this.helpers.handleError(err)
    );

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => this.helpers.handleError(err)
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');
    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = 'local-profile';
  }

  private loadAppConfig() {
    // console.log('loadAppConfig()');
    this.data.getAppConfig()
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (appConfig) => {
          // console.log('appConfig', appConfig);
          this.appConfig = appConfig;
        },
        (err) => this.helpers.handleError(err)
      );

    this.helpers.timePresentLoading('');
  }

  private loadUser() {
    // console.log('loadUser()');

    this.data.getUser().subscribe(
      (user: sae.IAuthUser) => this.user = user,
      (err) => this.helpers.handleError(err)
    );
  }

  async startQuestionnaire(qSetId: number, newOnly?: boolean) {
    // console.log('startQuestionnaire(qSetId, newOnly)', [qSetId, newOnly]);
    this.navCtrl.navigateForward('/question-list/0/' + qSetId, { queryParams: { newOnly } });
  }

  async aboutApp() {
    // console.log('aboutApp()');
    const toast = await this.toastCtrl.create({
      message: 'App version ' + config.appVersion,
      duration: 1000
    });
    await toast.present();
  }

  goToSettings() {
    // console.log('goToSettings()');

    this.navCtrl.navigateForward('/settings');
  }

  logOut() {
    // console.log('logOut()');

    this.events.publish('logout');
  }

  async becomeUser() {
    // console.log('becomeUser()');
    // console.log('this.user', this.user);

    if (this.user.terms_accepted_at != 'now') {
      this.askTermsAccept('becomeUser');
      return;
    }

    var alertOpts = {
      title: this.translate.instant('User registration'),
      message: this.translate.instant('To register please fill the fields below'),
      inputs: [
        {
          name: 'first_name',
          placeholder: this.translate.instant('First name'),
        },
        {
          name: 'last_name',
          placeholder: this.translate.instant('Last name'),
        },
        {
          name: 'email',
          placeholder: this.translate.instant('Email'),
          type: 'email',
        },
        {
          name: 'phone_code',
          placeholder: this.translate.instant('Country code'),
          type: 'tel',
        },
        {
          name: 'phone',
          placeholder: this.translate.instant('Phone number'),
          type: 'tel',
        },
      ] as AlertInput[],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: data => {
            // console.log('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('Register'),
          handler: data => this.handleRegistrationForm(data),
        }
      ],
      enableBackdropDismiss: false,
    };
    this.guestRegisterPrompt = await this.alertCtrl.create(alertOpts);
    await this.guestRegisterPrompt.present();
  }

  private async  handleRegistrationForm(data) {
    // console.log('handleRegistrationForm(data)', data);
    var toastMsg = this.validateRegistration(data);
    if (toastMsg) {
      const toast = await this.toastCtrl.create({
        message: toastMsg,
        duration: 3000,
      });
      await toast.present();
      return false;
    }
    // console.log('this.user', this.user);

    var userData = JSON.parse(JSON.stringify(this.user));
    userData.is_guest = false;
    userData = Object.assign(userData, data);
    // console.log('userData', userData);

    this.auth.updateUser(userData)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (user: sae.IAuthUser) => this.recognize(user.email),
        (err) => this.helpers.handleError(err),
      );
    this.helpers.timePresentLoading('');

    return false;
  }

  private validateRegistration(data: sae.IAuthRegisterSpecs): string {
    // console.log('validateRegistration(data)', data);

    if (!data.first_name) {
      return this.translate.instant('First name') + ' ' + this.translate.instant('is required');
    }
    if (!data.last_name) {
      return this.translate.instant('Last name') + ' ' + this.translate.instant('is required');
    }
    if (!this.emailRegex.test(data.email)) {
      return this.translate.instant('Email') + ' ' + this.translate.instant('is required');
    }
    if (!this.ccodeRegex.test(String(data.phone_code))) {
      return this.translate.instant('Country code') + ' ' + this.translate.instant('is required');
    }
    if (!this.phoneRegex.test(String(data.phone))) {
      return this.translate.instant('Phone number') + ' ' + this.translate.instant('is required');
    }
    return '';
  }

  private recognize(email: string) {
    // console.log('recognize(email)', email);

    let authReminderParams: sae.IAuthReminderParams = {
      email: email,
    };
    this.helpers.timePresentLoading('');
    this.auth.recognize(authReminderParams)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        () => this.logOutForVerification(email),
        (err) => this.helpers.handleError(err)
      );
  }

  private async logOutForVerification(email: string) {
    // console.log('logOutForVerification(email)', email);

    this.guestRegisterPrompt.dismiss();
    const toast = await this.toastCtrl.create({
      message: this.translate.instant('Verification is sent to you'),
      duration: 3000,
    });
    await toast.present();

    this.events.publish('logout', { verificationEmail: email });
  }

  private async askTermsAccept(caller?: string) {
    // console.log('askTermsAccept(caller)', caller);
    var acceptOpts = {
      title: this.translate.instant('Please read and accept'),
      message: this.appConfig.terms_and_conditions,
      inputs: [
        {
          type: 'checkbox',
          label: this.translate.instant('I understand and accept'),
          value: 'accepted',
          checked: false,
        },
      ] as AlertInput[],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: (data) => {
            // console.log('Cancel handler(data)', data);
          },
        },
        {
          text: this.translate.instant('Confirm'),
          handler: (data) => {
            // console.log('Accept handler(data)', data);
            if (data[0] != 'accepted') {
              return false;
            }
            this.user.terms_accepted_at = 'now';
            this[caller]();
            return true;
          },
        },
      ],
      enableBackdropDismiss: false,
    };

    const alert = await this.alertCtrl.create(acceptOpts);
    await alert.present();
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    if (config.profile_background) {
      css = css.concat('page-profile {background-image: url(' + config.profile_background + ')}');
    }

    if (config.profile_overlay_color) {
      css = css.concat('page-profile .content:before {background-color: ' + config.profile_overlay_color + '}');
    }

    if (config.profile_item_background_color) {
      css = css.concat('page-profile .content .list.question-sets .item-block {background-color: ' + config.profile_item_background_color + '}');
    }

    return scripts +
      '<style>' + css + '</style>'
      ;
  }
}
