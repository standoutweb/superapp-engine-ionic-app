import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositListPage } from './deposit-list.page';

describe('DepositListPage', () => {
  let component: DepositListPage;
  let fixture: ComponentFixture<DepositListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
