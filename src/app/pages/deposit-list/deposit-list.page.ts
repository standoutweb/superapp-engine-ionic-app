import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, } from '@ionic/angular';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-deposit-list',
  templateUrl: './deposit-list.page.html',
  styleUrls: ['./deposit-list.page.scss'],
})
export class DepositListPage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};

  isLoaded: boolean = false;

  private deposits: Array<sae.IDeposit> = [];
  private categories: Array<sae.ICategory> = [];

  private depositCategoryIndex: Array<sae.ICategory> = [];

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,

    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('DepositListPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        if (!sections.length) return;
        this.sections = sections;
      }
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secid = Number(this.route.snapshot.paramMap.get('secid'));
    if (!secid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (!this.section || this.section.id != secid) {
      this.section = this.sections.find((sec) => sec.id == secid);
      // console.log('section', this.section);
    }

    if (!this.section) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = this.section.id;

    //

    // fallback layout mode
    if (!this.section.params.mode) {
      this.section.params.mode = 'show-numbers';
    }

    this.isLoaded = false;
    this.helpers.timePresentLoading('');
    this.data.listDeposits(this.section.group_plugin_id).subscribe(
      (deposits: Array<sae.IDeposit>) => this.processDepositsAndCategories(deposits, null)
    );
    this.data.listDepositCategories(this.section.group_plugin_id).subscribe(
      (categories: Array<sae.ICategory>) => this.processDepositsAndCategories(null, categories)
    )
  }

  processDepositsAndCategories(deposits: Array<sae.IDeposit> = null, categories: Array<sae.ICategory> = null) {
    // console.log('processDepositsAndCategories(deposits, categories)', deposits, categories);
    if (deposits !== null) this.deposits = deposits;
    if (categories !== null) this.categories = categories;

    // something missing?
    if (!this.deposits || !this.categories) return;

    // requirements fetched
    this.isLoaded = true;
    this.helpers.timeDismissLoading();

    // combine
    var depositCategoryIndex: Array<sae.ICategory> = [];

    this.categories.forEach(
      (category) => {
        (category as any).deposits = [];
        this.deposits.forEach(
          (deposit) => {
            // if category matches, put in index
            if (deposit.categories[0].id === category.id) {
              (category as any).deposits.push(deposit);
            }
          }
        );

        // if there are deposits in the category, will show them
        if ((category as any).deposits.length) {
          depositCategoryIndex.push(category);
        }
      }
    );

    this.depositCategoryIndex = depositCategoryIndex;
    // console.log('this.depositCategoryIndex', this.depositCategoryIndex);
  }

  openDashboardSection() {
    // console.log('openDashboardSection()');
    this.events.publish('section:landing');
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    return scripts +
      '<style>' + css + '</style>'
      ;
  }

  doRefresh(event) {
    // console.log('doRefresh(event)', event);
    this.isLoaded = false;
    this.helpers.timePresentLoading('');
    event.complete();

    this.data.pullDeposits(this.section.group_plugin_id);
  }

}
