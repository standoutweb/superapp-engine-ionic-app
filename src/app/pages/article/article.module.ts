import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TranslateModule } from '@ngx-translate/core';

import { PipesModule } from '../../pipes/pipes.module';
import { ArticlePage } from './article.page';

const routes: Routes = [
  {
    path: '',
    component: ArticlePage
  }
];

@NgModule({
  declarations: [ArticlePage],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),

    IonicModule,

    TranslateModule,

    PipesModule,
  ],
})
export class ArticlePageModule { }
