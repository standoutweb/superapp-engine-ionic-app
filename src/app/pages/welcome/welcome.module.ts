import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TranslateModule } from '@ngx-translate/core';

import { WelcomePage } from './welcome.page';

const routes: Routes = [
  {
    path: '',
    component: WelcomePage
  },
];

@NgModule({
  declarations: [WelcomePage],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),

    IonicModule,

    TranslateModule,
  ],
})
export class WelcomePageModule { }
