import { Component, ViewChild, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, Events, IonSlides, NavController, ToastController, } from '@ionic/angular';
import { AlertInput, PopoverOptions, } from '@ionic/core';

import { Deeplinks, DeeplinkMatch } from '@ionic-native/deeplinks/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';
import { TranslateService } from '@ngx-translate/core';

import { config } from '../../config';
import { AuthService } from '../../services/auth/auth.service';
import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

import { countries } from '../../data/countries';

@Component({
  selector: 'app-welcome',
  templateUrl: 'welcome.page.html',
  styleUrls: ['welcome.page.scss'],
})
export class WelcomePage {
  features = config.features;

  countries = countries;
  countryPicker = {
    value: 'dk',
    interfaceOptions: { cssClass: 'country-picker-pop' } as PopoverOptions,
  };

  private appConfig: { [key: string]: string } = {};

  private reg = { phone_code: '45' } as sae.IAuthRegisterSpecs;

  private recognizeId: any = { phone_code: '45' };
  private recognizeQue: any = {};

  private verifyQue: any = {};

  private deeplinksMatch: any;
  private pushTokenSaved: boolean = false;

  @ViewChild(IonSlides, { static: false }) welcomeSlider: IonSlides;
  welcomeSliderOpts = {
    spaceBetween: 16,
  };

  private slidesMap: Array<string> = [];

  constructor(
    private route: ActivatedRoute,
    public alertCtrl: AlertController,
    public events: Events,
    public navCtrl: NavController,
    public toastCtrl: ToastController,

    private splashScreen: SplashScreen,
    private deeplinks: Deeplinks,

    public translate: TranslateService,

    public auth: AuthService,
    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('WelcomePage::constructor()');
    Object.keys(config.features).forEach(
      (featureKey) => {
        if (!config.features[featureKey]) return;
        this.slidesMap.push(featureKey);
      }
    );
  }

  ngOnInit() {
    // console.log('WelcomePage::ngOnInit()');

    this.events.subscribe(
      'loginpush',
      (authToken: string) => {
        // console.log('authToken', authToken);

        this.data.setAuthToken(authToken).subscribe(
          () => {
            this.auth.user()
              .finally(() => this.helpers.timeDismissLoading())
              .subscribe(
                (loginRes: sae.IAuthUser) => this.handleLoginResponse(Object.assign(loginRes, { token: authToken })),
                (err) => this.helpers.handleError(err)
              );
            this.helpers.timePresentLoading('');
          }
        );
      }
    );

    this.events.subscribe(
      'pushToken:saved',
      () => {
        this.pushTokenSaved = true;
        this.handleDeeplinks();
      }
    );

    this.registerDeeplinks();

    this.loadAppConfig();

    this.events.publish('rootPage:loaded');
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');
    this.data.state.menuEnabled = false;
  }

  async ionViewDidEnter() {
    // console.log('ionViewWillEnter()');
    const verificationEmail = this.route.snapshot.queryParamMap.get('verificationEmail');

    if (verificationEmail) {
      this.recognizeId.channel = 'email';
      this.handleRecognizeResponse(verificationEmail);
    }

    this.splashScreen.hide();
  }

  private loadAppConfig() {
    // console.log('loadAppConfig()');
    this.data.getAppConfig()
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (appConfig) => {
          // console.log('appConfig', appConfig);
          this.appConfig = appConfig;
        },
        (err) => this.helpers.handleError(err)
      );

    this.helpers.timePresentLoading('');
  }

  async slideWillChange() {
    // console.log('slideChanged()');
    let slideIndex = await this.welcomeSlider.getActiveIndex();
    // console.log('slideIndex', slideIndex);

    switch (this.slidesMap[slideIndex]) {
      case 'welcomeCreateEmailPhone':
        if (!this.reg.terms_accepted_at) {
          this.askTermsAccept(null, () => { }, () => this.goToSlide('welcomeWelcome'));
        }
        break;

      default:
        break;
    }
  }

  private goToSlide(featureKey: string) {
    // console.log('goToSlide(featureKey)', featureKey);
    this.welcomeSlider.slideTo(this.determineSlideIndex(featureKey));
  }

  private determineSlideIndex(featureKey: string): number {
    // console.log('determineSlideIndex(featureKey)', featureKey);
    // console.log('this.recognizeId', this.recognizeId);
    if (featureKey == 'welcomeRecognizeAny') {
      if (config.features['welcomeRecognizePhone'] && this.recognizeId.channel != 'email') {
        featureKey = 'welcomeRecognizePhone';
      } else {
        featureKey = 'welcomeRecognizeEmail';
      }
    }
    // console.log('featureKey', featureKey);
    // order of slides in welcome.html

    return this.slidesMap.indexOf(featureKey);
  }

  specHandleRegisterResponse(loginRes: sae.IAuthUser) {
    // console.log('specHandleRegisterResponse(loginRes)', loginRes);
    if (!loginRes || !loginRes.token) {
      this.helpers.handleError('Unknown error');
      return;
    }

    this.helpers.timePresentLoading('');
    let authReminderParams: sae.IAuthReminderParams = {};
    authReminderParams.email = loginRes.email;
    this.auth.recognize(authReminderParams)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        () => this.handleRecognizeAfterRegister(loginRes.email),
        (err) => this.helpers.handleError(err)
      );
  }

  async handleRecognizeAfterRegister(auth_field: string) {
    // console.log('handleRecognizeAfterRegister(auth_field)', auth_field);

    // prepare view for entering verification
    this.recognizeId.channel = 'email';
    this.handleRecognizeResponse(auth_field);

    // clear reg form
    this.reg.first_name = '';
    this.reg.last_name = '';
    this.reg.email = '';
    this.reg.phone_code = '';
    this.reg.phone = '';
    this.reg.terms_accepted_at = '';

    const toast = await this.toastCtrl.create({
      message: this.translate.instant('Verification is sent to you'),
      duration: 5000,
    });
    await toast.present();
  }

  async recognize(channel: string) {
    // console.log('recognize(channel)', channel);

    this.recognizeId.channel = channel;
    let otherChannel = (channel == 'phone' ? 'email' : 'phone');
    this.recognizeId[otherChannel] = '';
    // console.log('this.recognizeId', this.recognizeId);

    let auth_field = this.recognizeId[channel];
    // console.log('auth_field', auth_field);

    if (!auth_field) {
      const toast = await this.toastCtrl.create({
        message: this.translate.instant('Please fill a valid ' + channel),
        duration: 5000,
      });
      await toast.present();
      return;
    }

    if (this.recognizeId.channel == 'phone') {
      auth_field = '+' + this.recognizeId.phone_code + auth_field;
    }

    // workaround, if wrong field used
    if (auth_field.includes('@') && channel === 'phone') {
      channel = 'email';
      this.recognizeId.channel = 'email';
      this.recognizeId.phone = '';
      this.recognizeId.email = auth_field;
    }

    // Apple review special case - don't send request to regenerate verification
    if (auth_field.includes('+AppleReview')) {
      this.handleRecognizeResponse(auth_field);
      return;
    }

    this.helpers.timePresentLoading('');
    let authReminderParams: sae.IAuthReminderParams = {};
    authReminderParams[channel] = auth_field;
    // console.log('authReminderParams', authReminderParams);
    this.auth.recognize(authReminderParams)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        () => this.handleRecognizeResponse(auth_field),
        (err) => this.helpers.handleError(err)
      );
  }

  private handleRecognizeResponse(auth_field: string) {
    // console.log('handleRecognizeResponse(auth_field)', auth_field);
    this.recognizeQue.auth_field = auth_field;
    this.verifyQue.verification = '';
    this.goToSlide('welcomeVerificationCode');
  }

  verify() {
    // console.log('verify()');
    this.verifyQue.auth_field = this.recognizeQue.auth_field;
    this.verifyQue.channel = this.recognizeId.channel;
    // console.log('verifyQue', this.verifyQue);
    this.auth.verify(this.verifyQue)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (loginRes: sae.IAuthUser) => this.handleLoginResponse(loginRes),
        (err) => this.helpers.handleError(err)
      );
    this.helpers.timePresentLoading('');
  }

  registerWithEmailAndPhone() {
    // console.log('registerWithEmailAndPhone()');
    // console.log('this.reg', this.reg);

    if (!this.reg.terms_accepted_at) {
      this.askTermsAccept(null, this.registerWithEmailAndPhone);
      return;
    }

    this.reg.password = Math.random().toString(36);
    this.auth.registerWithEmailAndPhone(this.reg)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (loginRes: sae.IAuthUser) => this.specHandleRegisterResponse(loginRes),
        (err) => this.helpers.handleError(err)
      );
    this.helpers.timePresentLoading('');
  }

  logInWithFacebook() {
    // console.log('logInWithFacebook()');
    // console.log('this.reg', this.reg);

    if (!this.reg.terms_accepted_at) {
      this.askTermsAccept(null, this.logInWithFacebook);
      return;
    }

    this.auth.logInWithFacebook(this.reg)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (loginRes: sae.IAuthUser) => this.handleLoginResponse(loginRes),
        (err) => this.helpers.handleError(err)
      );
    this.helpers.timePresentLoading('');
  }

  // TODO: Abstract this away in Auth or Appsup
  private handleLoginResponse(loginRes: sae.IAuthUser, groupId?: number) {
    // console.log('handleLoginResponse(loginRes)', loginRes);
    if (!loginRes || !loginRes.token) {
      this.helpers.handleError(loginRes);
      return;
    }

    // else proceed
    this.data.setAuthToken(loginRes.token);

    // console.log('loginRes.terms_accepted_at', loginRes.terms_accepted_at);
    // console.log('this.appConfig.terms_and_conditions_updated_at', this.appConfig.terms_and_conditions_updated_at);
    if (!loginRes.terms_accepted_at || loginRes.terms_accepted_at < this.appConfig.terms_and_conditions_updated_at) {
      // console.log('terms not accepted');
      this.askTermsAccept(loginRes);
      return;
    }
    // console.log('all good');

    this.events.publish('user:loaded', loginRes);

    // "reload"
    var urlPath = '/groups';
    if (groupId) urlPath += '/' + groupId;
    this.navCtrl.navigateRoot(urlPath);
  }

  private updateUserTermsAccepted(auUser: sae.IAuthUser) {
    // console.log('updateUserTermsAccepted(auUser)', auUser);
    auUser.terms_accepted_at = 'now';
    this.auth.updateUser(auUser)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (updatedAuUser: sae.IAuthUser) => this.handleLoginResponse(updatedAuUser),
        (err) => this.helpers.handleError(err)
      );
    this.helpers.timePresentLoading('');
  }

  registerAsGuest() {
    // console.log('registerAsGuest()');
    // console.log('this.reg', this.reg);

    if (!this.reg.terms_accepted_at) {
      this.askTermsAccept(null, this.registerAsGuest);
      return;
    }

    this.auth.registerAsGuest(this.reg)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (loginRes: sae.IAuthUser) => this.handleLoginResponse(loginRes),
        (err) => this.helpers.handleError(err)
      );
    this.helpers.timePresentLoading('');
  }

  private registerDeeplinks() {
    // console.log('registerDeeplinks()');
    this.deeplinks.route({
      '/deeplinks/login': true,
      // TODO: this is workaround
      '/mobile/dashboard': true,
    }).subscribe(
      (match: DeeplinkMatch) => {
        this.deeplinksMatch = match;
        this.helpers.timePresentLoading('');
        this.handleDeeplinks();
      },
      (nomatch) => console.error('Got a deeplink that didn\'t match', nomatch)
    );
  }

  private handleDeeplinks() {
    // console.log('handleDeeplinks(this.deeplinksMatch)', this.deeplinksMatch);
    if (!this.deeplinksMatch || !this.pushTokenSaved) {
      // console.log('not ready yet - exiting');
      return;
    }
    this.helpers.timeDismissLoading();
    // match.$route - the route we matched, which is the matched entry from the arguments to route()
    // match.$args - the args passed in the link
    // match.$link - the full link data
    var loginData = {
      magicKey: this.deeplinksMatch.$args.magic_key
    };
    this.auth.logInWithMagicKey(loginData)
      .subscribe(
        (loginRes: sae.IAuthUser) => this.handleLoginResponse(loginRes, this.deeplinksMatch.$args.group_id),
        (err) => this.helpers.handleError(err)
      );
  }

  async askTermsAccept(auUser: sae.IAuthUser, acceptCallback = () => { }, cancelCallback = () => { }) {
    // console.log('askTermsAccept(auUser, acceptCallback, cancelCallback)', auUser, acceptCallback, cancelCallback);
    var acceptOpts = {
      title: this.translate.instant('Please read and accept'),
      message: (this.appConfig ? this.appConfig.terms_and_conditions : ''),
      inputs: [
        {
          type: 'checkbox',
          label: this.translate.instant('I understand and accept'),
          value: 'accepted',
          checked: false,
        },
      ] as AlertInput[],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          handler: (data) => {
            // console.log('Cancel handler(data)', data);
            cancelCallback.call(this);
          },
        },
        {
          text: this.translate.instant('Confirm'),
          handler: (data) => {
            // console.log('Accept handler(data)', data);
            if (data[0] != 'accepted') {
              return false;
            }
            if (auUser) {
              this.updateUserTermsAccepted(auUser);
            } else {
              this.reg.terms_accepted_at = 'now';
              acceptCallback.call(this);
            }
            return true;
          },
        },
      ],
      enableBackdropDismiss: false,
    };

    const alert = await this.alertCtrl.create(acceptOpts);
    await alert.present();
  }

  onCountryPicked(event: CustomEvent, pageProperty: string) {
    // console.log('onCountryPicked(event, pageProperty)', event, pageProperty);
    // console.log('this.countries', this.countries);
    let country = this.countries.find((c) => event.detail.value == c.iso2);
    // console.log('country', country);
    this[pageProperty].phone_code = String(country.dialCode);
  }

}
