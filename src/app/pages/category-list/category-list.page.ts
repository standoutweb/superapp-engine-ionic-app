import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, } from '@ionic/angular';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

import { config } from '../../config';
import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.page.html',
  styleUrls: ['./category-list.page.scss'],
})
export class CategoryListPage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};

  isLoaded: boolean = false;

  private group_plugin_id: number;
  private category_id: number;
  private categoryList: Array<sae.ICategory> = [];

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,

    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('CategoryListPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        if (!sections.length) return;
        this.sections = sections;
      }
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secid = Number(this.route.snapshot.paramMap.get('secid'));
    if (!secid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (!this.section || this.section.id != secid) {
      this.section = this.sections.find((sec) => sec.id == secid);
      // console.log('section', this.section);
    }

    if (!this.section) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = this.section.id;

    //

    var pluginid = Number(this.section.group_plugin_id);

    if (!pluginid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    var catid = Number(this.route.snapshot.paramMap.get('catid'));
    if (!catid && this.section.params.catalog_category_id) {
      catid = Number(this.section.params.catalog_category_id);
    }

    if (this.group_plugin_id == pluginid) {
      if (this.category_id == catid) {
        // same article list already set - all good
        return;
      }
    }

    this.group_plugin_id = pluginid;
    this.category_id = catid;

    this.isLoaded = false;
    this.data.listCategories(this.group_plugin_id, this.category_id).subscribe(
      (categoryList: Array<sae.ICategory>) => {
        // console.log('categoryList', categoryList);
        this.helpers.timePresentLoading('');
        if (!categoryList) {
          categoryList = [];
        } else {
          if (categoryList.length == 1) {
            this.openCategory(categoryList[0], 'redirect');
          } else {
            this.categoryList = categoryList;
          }
          // Delay to fix flickering
          setTimeout(() => { this.isLoaded = true; }, 300);
          this.helpers.timeDismissLoading();
        }
      }
    );
  }

  openCategory(category: sae.ICategory, mode = 'open') {
    // console.log('openCategory(category, mode)', [category, mode]);
    var urlPath: string;
    if (category.has_children) {
      urlPath = '/category-list';
    } else {
      urlPath = '/artcile-list';
    }

    if (mode == 'redirect') {
      // prevent back-navigation loop
      this.navCtrl.navigateRoot(urlPath + '/' + this.section.id + '/' + category.id);
    } else {
      this.navCtrl.navigateForward(urlPath + '/' + this.section.id + '/' + category.id);
    }
  }

  openDashboardSection() {
    // console.log('openDashboardSection()');
    this.events.publish('section:landing');
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    return scripts +
      '<style>' + css + '</style>'
      ;
  }

  doRefresh(event) {
    // console.log('doRefresh(event)', event);
    this.isLoaded = false;
    this.helpers.timePresentLoading('');
    event.target.complete();

    this.data.pullCategories(this.group_plugin_id, this.category_id);
  }

}
