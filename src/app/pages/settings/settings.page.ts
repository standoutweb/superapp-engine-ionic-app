import { Component, OnInit, } from '@angular/core';
import { AlertController, Events, NavController, ToastController, } from '@ionic/angular';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';
import { TranslateService } from '@ngx-translate/core';

import { config } from '../../config';
import { AuthService } from '../../services/auth/auth.service';
import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  features = config.features;

  private user: any = {};
  public base64Image: string;

  constructor(
    public alertCtrl: AlertController,
    public events: Events,
    public navCtrl: NavController,
    public toastCtrl: ToastController,

    private camera: Camera,

    public translate: TranslateService,

    public auth: AuthService,
    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('SettingsPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getUser().subscribe(
      (user: sae.IAuthUser) => {
        // console.log('user', user);
        if (!user.is_guest) {
          if ((user as any).image) {
            this.base64Image = (user as any).image;
          } else {
            this.base64Image = 'assets/user.png';
          }
          this.user = user;
        }
        this.events.publish('user:loaded', user);
      },
      (err) => this.helpers.handleError(err)
    );
  }

  async ionViewWillEnter() {
    // console.log('ionViewWillEnter()');
  }

  // TODO: Abstract this away in Auth or Appsup
  private async handleUserUpdateResponse(loginRes: sae.IAuthUser) {
    // console.log('handleUserUpdateResponse(loginRes)', loginRes);
    if (!loginRes || !loginRes.token) {
      this.helpers.handleError(loginRes);
      return;
    }

    // else proceed
    this.data.setAuthToken(loginRes.token);
    this.data.setUser(loginRes);
    this.events.publish('user:loaded', loginRes);

    const toast = await this.toastCtrl.create({
      message: this.translate.instant('Your profile was updated'),
      duration: 3000,
    });
    await toast.present();
  }

  dismiss() {
    // console.log('dismiss()');
    this.navCtrl.pop();
  }

  submit() {
    // console.log('submit()');
    // console.log('this.user', this.user);

    this.auth.updateUser(this.user)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (response: sae.IAuthUser) => this.handleUserUpdateResponse(response),
        (err) => this.helpers.handleError(err)
      );
    this.helpers.timePresentLoading('');
  }

  updateImage(sourceType: number) {
    // console.log('updateImage(sourceType)', sourceType);
    let cameraOptions: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: sourceType,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 1000,
      targetHeight: 1000,
      cameraDirection: 1,
      correctOrientation: true,
    };
    this.camera.getPicture(cameraOptions).then(
      (imageData: string) => {
        // console.log('imageData', imageData);
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.user.image = this.base64Image;
        // TODO: Ask for user confirmation here
        this.submit();
      },
      (err) => this.helpers.handleError(err)
    );
  }

  logOut() {
    // console.log('logOut()');

    this.events.publish('logout');
  }

  async exportData() {
    // console.log('exportData()');
    let alertOpts = {
      title: this.translate.instant('Please confirm'),
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          role: 'cancel',
        },
        {
          text: this.translate.instant('Confirm'),
          handler: () => this.handleExportDataSubmit(),
        },
      ],
      enableBackdropDismiss: false,
    };
    const alert = await this.alertCtrl.create(alertOpts);
    await alert.present();
  }

  private handleExportDataSubmit() {
    // console.log('handleExportDataSubmit()');

    this.auth.exportData().subscribe(
      async (res) => {
        // console.log('res', res);
        const toast = await this.toastCtrl.create({
          message: this.translate.instant('We will prepare your data and send an archive to your email'),
          duration: 3000,
        });
        await toast.present();
      },
      (err) => this.helpers.handleError(err)
    );
  }

  async deleteUser() {
    // console.log('deleteUser()');
    let alertOpts = {
      title: this.translate.instant('Please type `DELETE` and confirm'),
      inputs: [
        {
          name: 'phrase',
        },
      ],
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          role: 'cancel',
        },
        {
          text: this.translate.instant('Confirm'),
          handler: (data) => this.handleDeleteUserSubmit(data),
        },
      ],
      enableBackdropDismiss: false,
    };
    const alert = await this.alertCtrl.create(alertOpts);
    await alert.present();
  }

  private async handleDeleteUserSubmit(data) {
    // console.log('handleDeleteUserSubmit(data)', data);
    if (data.phrase !== 'DELETE') {
      // console.log('will NOT delete');
      const toast = await this.toastCtrl.create({
        message: this.translate.instant('You have to type `DELETE`'),
        duration: 3000,
      });
      await toast.present();
      return false;
    }

    // console.log('will delete');
    this.auth.deleteUser().subscribe(
      async () => {
        const toast = await this.toastCtrl.create({
          message: this.translate.instant('Your profile is deleted'),
          duration: 3000,
        });
        await toast.present();

        this.logOut();
      },
      (err) => this.helpers.handleError(err)
    );
  }
}
