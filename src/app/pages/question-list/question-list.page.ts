import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, ModalController, ToastController, } from '@ionic/angular';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../services/auth/auth.service';
import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.page.html',
  styleUrls: ['./question-list.page.scss'],
})
export class QuestionListPage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};

  // Subscription
  private qSetsSub;

  private qSet: sae.ICategory;
  private newOnly: boolean = true;
  private question: sae.IAttribute;
  private answer: {};
  private showMetaScreen: string = '';

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,
    public toastCtrl: ToastController,

    public translate: TranslateService,

    public auth: AuthService,
    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('QuestionListPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        if (!sections.length) return;
        this.sections = sections;
      }
    );
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secidSegment = this.route.snapshot.paramMap.get('secid');
    let secid = Number(secidSegment);

    if (!secid && secidSegment !== '0') {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (secidSegment === '0') {
      this.section = null;
    } else {
      if (!this.section || this.section.id != secid) {
        this.section = this.sections.find((sec) => sec.id == secid);
        // console.log('section', this.section);
      }
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = null;

    var catid = Number(this.route.snapshot.paramMap.get('catid'));
    this.newOnly = Boolean(this.route.snapshot.queryParamMap.get('newOnly'));
    // console.log('newOnly', this.newOnly);

    if (this.section) {
      catid = this.section.params.user_attribute_category_id;
      this.newOnly = this.section.params.only_new;
    }

    this.determineQuestionary(catid);
  }

  determineQuestionary(qSetId?: number) {
    // console.log('determineQuestionary(qSetId)', qSetId);

    this.qSetsSub = this.data.getQuestionSets().subscribe(
      (qSets: Array<sae.ICategory>) => {
        // console.log('qSets', qSets);
        if (!qSets) {
          // Skip initial value from shared Observable?
          return;
        }

        if (!qSetId && qSets[0]) {
          // no designated questionary - take first
          this.initQuestionary(qSets[0]);
          return;
        }

        // look for designated questionary
        for (var i = qSets.length - 1; i >= 0; i--) {
          // console.log('qSets[i]', qSets[i]);
          if (qSets[i].id == qSetId) {
            // console.log('qSetId ' + qSetId + ' found at i ' + i);
            this.initQuestionary(qSets[i]);
            return;
          }
        }

        // questionary not found - dismiss
        this.dismiss();
      },
      (err) => this.helpers.handleError(err)
    );
  }

  initQuestionary(questionary: sae.ICategory) {
    // console.log('initQuestionary(questionary)', questionary);

    this.qSet = questionary;

    if ((questionary as any).unanswered_questions == 0 && this.newOnly) {
      this.showMetaScreen = 'outro';
    } else {
      this.showMetaScreen = 'intro';
    }
  }

  showNextQuestion(qSetId: number) {
    // console.log('showNextQuestion(qSetId)', qSetId);

    // TODO: Need Observable here
    if (this.question && this.question.id) {
      var questionId = this.question.id;
    }
    // console.log('section', this.section);

    this.data.getNextQuestion(this.group.id, qSetId, this.newOnly, questionId)
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (question: sae.IAttribute) => {
          // console.log('question', question);
          if (question && question.id) {
            // console.log('will show question screen');
            this.showMetaScreen = '';
            // set active question object
            this.question = question;
            // console.log('question', this.question);
            this.answer = this.encodeAnswer((question as any).answers, question);
            // console.log('answer', this.answer);
          } else {
            // console.log('will show outro screen');
            this.showMetaScreen = 'outro';
          }
        },
        (err) => this.helpers.handleError(err)
      );
    this.helpers.timePresentLoading('');
  }

  dismiss() {
    // console.log('dismiss()');
    this.data.pullQuestionSets();
    this.navCtrl.pop();
  }

  async submit() {
    // console.log('submit()', this.answer);
    // console.log('question', this.question);
    var ans = this.decodeAnswer(this.answer, this.question);
    // console.log('ans', ans);
    // TODO: Ask confirmation before saving empty answer
    if (ans && String(ans).length) {
      // console.log('submit answer');
      var answerPackage: sae.IAuthUpdateSpecs = {};
      answerPackage[this.question.slug] = ans;
      // console.log('answerPackage', answerPackage);
      this.auth.updateUser(answerPackage).subscribe(
        (res) => {
          // console.log('res', res);
          this.events.publish('user:loaded', res);

          // TODO: assuming there is single category
          this.showNextQuestion(this.question.categories[0].id);
        },
        (err) => this.helpers.handleError(err)
      );
    } else {
      // console.log('skip to next question');
      // TODO: assuming there is single category
      this.showNextQuestion(this.question.categories[0].id);

      const toast = await this.toastCtrl.create({
        message: this.translate.instant('Answer was not saved'),
        duration: 2000,
      });
      await toast.present();
    }
  }

  private encodeAnswer(answer: Array<string>, attribute: sae.IAttribute): any {
    // console.log('encodeAnswer(answer, attribute)', [answer, attribute]);

    if (attribute.type === 'options' && !attribute.is_collection) {
      return answer;
    }

    if (attribute.type === 'options' && attribute.is_collection) {
      var assoc = {};
      var len = answer.length;
      for (var i = 0; i < len; i++) {
        // console.log('i, answer[i]', [i, answer[i]]);
        assoc[String(answer[i])] = true;
      }
      // console.log('assoc', assoc);
      return assoc;
    }
    // TODO: Remote logging
    this.helpers.handleError('Unhandled attribute type');
    return;
  }

  private decodeAnswer(answer: any, question: sae.IAttribute): any {
    // console.log('decodeAnswer(answer, question)', [answer, question]);

    if (question.type === 'options' && !question.is_collection) {
      return answer;
    }

    if (question.type === 'options' && question.is_collection) {
      // dict with bool true to array
      var arr: number[] = [];
      arr = Object.keys(answer)
        // filter in only where checked true
        .filter((k: string) => answer[k])
        // we need integers
        .map((v) => Number(v))
        ;
      return arr;
    }

    this.helpers.handleError('Unhandled internal answer type');
    return;
  }

  addRangeClass($event) {
    var clickedStar = $event;
    var g = document.getElementsByClassName('stars')[0];

    for (var i = 0, len = g.children.length; i < len; i++) {
      g.children[i].classList.remove('in_range');
    }

    for (var j = 0, select = clickedStar; j < select; j++) {
      g.children[j].classList.add('in_range');
    }
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    if (config.questionary_button_background) {
      css = css.concat('.button {background-color: ' + config.questionary_button_background + '}');
    }

    if (config.questionary_button_background_activated) {
      css = css.concat('.button.activated {background-color: ' + config.questionary_button_background_activated + '}');
    }

    return scripts +
      '<style>' + css + '</style>'
      ;
  }
}
