import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogListPage } from './catalog-list.page';

describe('CatalogListPage', () => {
  let component: CatalogListPage;
  let fixture: ComponentFixture<CatalogListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
