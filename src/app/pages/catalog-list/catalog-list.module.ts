import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';

import { PipesModule } from '../../pipes/pipes.module';
import { CatalogListPage } from './catalog-list.page';

const routes: Routes = [
  {
    path: '',
    component: CatalogListPage
  }
];

@NgModule({
  declarations: [CatalogListPage],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),

    IonicModule,

    FontAwesomeModule,
    TranslateModule,

    PipesModule,
  ],
})
export class CatalogListPageModule { }
