import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, NavController, } from '@ionic/angular';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';

import { AuthService } from '../../services/auth/auth.service';
import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  private group: sae.IGroup;
  private sections: sae.IDestination[] = [];
  private section: sae.IDestination;

  styles: string = '';
  config: sae.IAttributeValues = {};

  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public navCtrl: NavController,

    public auth: AuthService,
    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('DashboardPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.data.getActiveGroup().then(
      (group: sae.IGroup) => {
        // console.log('group', group);
        this.group = group;
        this.config = group.attributes;
        // console.log('config', this.config);
        this.styles = this.renderStyles(group.attributes);
      },
      (err) => console.error(err)
    );

    this.data.getSections().subscribe(
      (sections) => {
        this.helpers.timeDismissLoading();
        if (!sections.length) return;
        this.sections = sections;
      }
    );

    // needed for badges in menu...
    // TODO: Is this realy needed?
    this.data.pullQuestionSets();

    this.events.publish('rootPage:loaded');
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    let secid = Number(this.route.snapshot.paramMap.get('secid'));
    if (!secid) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    if (!this.sections.length) {
      // wait for sections to finish loading
      setTimeout(() => this.ionViewWillEnter(), 200);
      return;
    }

    if (!this.section || this.section.id != secid) {
      this.section = this.sections.find((sec) => sec.id == secid);
      // console.log('section', this.section);
    }

    if (!this.section) {
      // console.log('cant do anything with this - go back');
      this.navCtrl.pop();
      return;
    }

    this.data.state.menuEnabled = true;
    this.data.state.menuCurrent = this.section.id;
  }

  ionViewDidEnter() {
    // console.log('ionViewDidEnter()');
  }

  openSection(sectionConf: sae.IDestination) {
    // console.log('openSection(sectionConf)', sectionConf);
    this.events.publish('section:open', sectionConf);
  }

  isSectionInDash(sectionConf: sae.IDestination) {
    // console.log('isSectionInDash(sectionConf)', sectionConf);

    if (!sectionConf.enabled) return false;

    if (sectionConf.component == 'DashboardPage') return false;

    if (sectionConf.component == 'MessageListPage') return false;

    if (sectionConf.component == 'GroupsPage') return false;

    return true;
  }

  renderStyles(config: sae.IAttributeValues): string {
    // console.log('renderStyles(config)', config);
    var scripts = '';
    var css = '';

    if (config.main_background) {
      css += `
        page-dashboard.ion-page {
          background-image: url("${config.main_background}");
        }
      `;
    }

    if (config.main_background_overlay) {
      css += `
        page-dashboard.ion-page:before {
          background-color: ${config.main_background_overlay};
        }
      `;
    }

    if (config.dashboard_header_background) {
      css = css.concat('page-dashboard ion-header {background-color: ' + config.dashboard_header_background + '}');
    }

    if (config.dashboard_item_radius) {
      css = css.concat('page-dashboard .item {border-radius: ' + config.dashboard_item_radius + '}');
    }

    if (config.dashboard_item_border) {
      css = css.concat('page-dashboard .content .item {border: ' + config.dashboard_item_border + '}');
    }

    if (config.dashboard_item_color) {
      css = css.concat('page-dashboard h3 {color: ' + config.dashboard_item_color + ' !important;}');
    }

    if (config.dashboard_item_icon_color) {
      css = css.concat('page-dashboard .content .icon {color: ' + config.dashboard_item_icon_color + '}');
    }

    if (config.dashboard_text_transform) {
      css = css.concat('page-dashboard h3 {text-transform: ' + config.dashboard_text_transform + '}');
    }

    return scripts +
      '<style>' + css + '</style>'
      ;
  }

  doRefresh(event) {
    // console.log('doRefresh(event)', event);
    this.helpers.timePresentLoading('');
    event.target.complete();

    this.data.pullSections();
  }

}
