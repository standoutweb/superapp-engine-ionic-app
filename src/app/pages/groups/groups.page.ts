import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events, } from '@ionic/angular';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import * as sae from 'superapp-engine-api-typescript-client/dist/common/interfaces';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../../services/auth/auth.service';
import { DataService } from '../../services/data/data.service';
import { HelpersService } from '../../services/helpers/helpers.service';

@Component({
  selector: 'app-groups',
  templateUrl: 'groups.page.html',
  styleUrls: ['groups.page.scss'],
})
export class GroupsPage implements OnInit {
  private groups: Array<sae.IGroup> = [];

  private groupId: number = 0;

  private revealGroups: boolean = false;

  constructor(
    private route: ActivatedRoute,
    public events: Events,

    private splashScreen: SplashScreen,

    public translate: TranslateService,

    public auth: AuthService,
    public data: DataService,
    public helpers: HelpersService,
  ) {
    // console.log('GroupsPage::constructor()');
  }

  ngOnInit() {
    // console.log('ngOnInit()');

    this.groupId = Number(this.route.snapshot.paramMap.get('groupid'));
    // console.log('groupId', this.groupId);

    this.data.listGroups()
      .finally(() => this.helpers.timeDismissLoading())
      .subscribe(
        (groups: Array<sae.IGroup>) => {
          // console.log('groups', groups);
          this.groups = groups;
          var glen = groups.length;
          var gcount = 0;
          var giAllowed = 0;
          for (var gi = 0; gi < glen; gi++) {
            // console.log('gi, groups[gi]', gi, groups[gi]);
            // console.log('is_member', (groups[gi] as any).is_member);
            // count groups - listed as public
            if (groups[gi].public || (groups[gi] as any).is_member) {
              // console.log('group allowed', groups[gi]);
              giAllowed = gi;
              gcount++;
            }
            // switch directly to designated group if any
            if (groups[gi].id == this.groupId) {
              // console.log('switch to designated group');
              this.switchGroup(groups[gi]);
              return;
            }
          }
          // check if only one visibly listed 
          if (gcount == 1) {
            // console.log('swicth to single allowed group');
            this.switchGroup(groups[giAllowed]);
          } else {
            // console.log('Let user choose a Group');
            this.revealGroups = true;
          }
        },
        (err) => console.error(err)
      );
    this.helpers.timePresentLoading('');

    this.events.publish('rootPage:loaded');
  }

  ionViewWillEnter() {
    // console.log('ionViewWillEnter()');

    this.data.clearObservers();
    this.events.publish('group:switched', null);

    this.data.state.menuEnabled = false;
    this.data.state.menuCurrent = 'local-groups';
  }

  ionViewDidEnter() {
    // console.log('ionViewDidEnter()');
    this.splashScreen.hide();
  }

  private switchGroup(groupConf: sae.IGroup) {
    // console.log('switchGroup(groupConf)', groupConf);

    // If member, then just go in
    if ((groupConf as any).is_member) {
      this.events.publish('group:switched', groupConf);
      return;
    }
    // console.log('not member yet');
    // If protected 
    // TODO: Maybe request can be sent and moderated in BE
    if (groupConf.protected) {
      this.helpers.presentAlert(this.translate.instant('Group is protected'), this.translate.instant('You have to be invited to get into this group'), this.translate.instant('OK'));
      return;
    }
    // join group
    this.performRegistration(groupConf);
  }

  performRegistration(groupConf: sae.IGroup) {
    // console.log('performRegistration(groupConf)', groupConf);
    // TODO: Philosofical - Should we require group participation to get questions?
    this.auth.joinGroup(groupConf.id).subscribe(
      async () => {
        // console.log('joinRes');
        this.events.publish('group:switched', groupConf);
        this.events.publish('section:landing');
      },
      (err) => console.error(err)
    );
  }

}
